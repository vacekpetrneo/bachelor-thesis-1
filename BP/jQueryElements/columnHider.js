/*
Author: Petr Vacek
Version: 0.1.0 2014-05-17
*/
$.fn.columnHider = function() {  
  var tableId = this.attr('id');
  var containerId = tableId + '-columnHider';
  var container = '<div id="' + containerId + '" style="display:none"></div>';
  var buttonClickFunction = "$('#" + containerId + "').toggle()"; 
  var button = '<span onClick="' + buttonClickFunction + '" style="text-decoration: underline; cursor: pointer; font-size: 75%">(vybrat sloupce)</span>';   
  this.before(button);
  this.before(container);
  this.children('thead').children().children()
    .each( function(index, element) {
      $('#' + containerId ).append('<div><input id="' + containerId + '-column' + index + '" type="checkbox"><label for="' + containerId + '-column' + index + '">Skrýt sloupec "' + element.innerHTML + '"</label></div>');
      $('#' + containerId + '-column' + index)
        .on('click', function(event){
          $('#' + tableId + ' thead tr th:nth-child(' + ($(event.target).parent().index()+1) + ')')
            .add('#' + tableId + ' tbody tr td:nth-child(' + ($(event.target).parent().index()+1) + ')')
            .toggle(!event.target.checked);      
        })
    }
  )
}
