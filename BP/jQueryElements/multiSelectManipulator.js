/*
Author: Petr Vacek
Version: 0.1.0 2014-05-17

*/
$.fn.multiSelectManipulator = function() {
  this.each(function(index, element){
    element = $(element);
    var id = element.attr('id');
    element.after('<div class="multiSelectManipulator">Označit <a href="" id="' + id + '-multiSelectManipulator-all">vše</a> / <a href="" id="' + id + '-multiSelectManipulator-none">nic</a></div>');
    $('#' + id + '-multiSelectManipulator-all')
      .on('click', function(event){
        element.find('option').prop('selected', true);
        element.trigger('change');        
        event.preventDefault();        
      });
    $('#' + id + '-multiSelectManipulator-none')
      .on('click', function(event){
        element.find('option').prop('selected', false);
        element.trigger('change');
        event.preventDefault();        
      });
  })
};
