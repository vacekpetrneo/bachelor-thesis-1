<?php

namespace SCG;

class SkupinaRepository extends Repository {

	/**
	 * @return NRow
	 */
	public function findById($skupinaID) {
		return $this->findBy(array('skupina_id' => $skupinaID ))->fetch();
	}

  /**
   * @return Nette\Database\Table\Selection
   */     
  public function fetchForCinnosti() {
    return $this->findAll()->where("typ = 1");
  }
    
}

