<?php

namespace SCG;

use Nette;

class TymRepository extends Repository {

  const SOUTEZICI_TABLE = 'soutezici';
  const SOUPISKA_TABLE = 'soupiska';
  
	/**
	 * Vrací všechny řádky z tabulky.
	 * @return Nette\Database\Table\Selection
	 */
	public function findAll() {
		return $this->getTable()
                ->select("tym.*")
                ->select("skola.nazev AS skola_nazev")
                ->select("akce.nazev AS akce_nazev")
                ->where("tym.skola_id = skola.skola_id")
                ->where("tym.akce_id = akce.akce_id");
	}

	/**
	 * @return  Nette\Database\Row
	 */
	public function findById($TymID) {
		return $this->findAll()->where("tym_id = ? ", $TymID)->fetch();
	}

  public function fetchPossiblePotvrzeno() {
    return array( 0 => "nepotvrzeno",
                  1 => "Potvrdil učitel",
                  2 => "Potvrdil koordinátor");
  }
	
  public function getTymsByTurnajId($TurnajId) {
    return $this->findAll()
                ->where(":" . self::SOUPISKA_TABLE . ".turnaj_id = ?", $TurnajId)
                ->order("tym.nazev");
  }
  
  public function getSouteziciByTymId($tymId) {
    return $this->table(self::SOUTEZICI_TABLE)
                ->where(self::SOUTEZICI_TABLE . '.tym_id', $tymId )
                ->order(self::SOUTEZICI_TABLE . '.kapitan DESC, '
                        .self::SOUTEZICI_TABLE . '.nahradnik');
  }

}