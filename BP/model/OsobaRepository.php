<?php

namespace SCG;

use Nette, Nette\Diagnostics\Debugger;

class OsobaRepository extends Repository {

	const TYPY_TABLE = 'kontaktni_osoba_typ';
  const AKCE_TABLE = 'kontaktni_osoba_akce';

  protected $kontaktni_osoba_types;

	public function __construct(Nette\Database\Context $db) {
    parent::__construct($db);  
    
    $this->kontaktni_osoba_types = $this->connection
                                  ->table(self::TYPY_TABLE)
                                  ->order('nazev ASC')
                                  ->fetchPairs('kontaktni_osoba_typ_id', 'nazev');    
  }

	/**
	 * @return string
	 */
	protected function getTableName() {
		return 'kontaktni_osoba';
	}

	/**
	 * @return  Nette\Database\Table\ActiveRow
	 */
	public function findAll() {
		return $this->getTable()
                ->select('kontaktni_osoba.*')
                ->select('kontaktni_osoba_typ.nazev AS kontaktni_osoba_typ_nazev')
                ->where('kontaktni_osoba_typ.kontaktni_osoba_typ_id
                          =
                          kontaktni_osoba.kontaktni_osoba_typ_id');
	}

	/**
	 * @param  array(int) | int $id
	 * @return Nette\Database\Table\Selection | Nette\Database\Table\ActiveRow
	 */
	public function findByID($id) {
    $result = $this->findAll()->where('kontaktni_osoba_id', $id);
    if (is_array($id)) {
      return $result;
    }
    else {
  		return $result->fetch();
    }
	}

  /**
   * @return Nette\Database\Table\Selection
   */     
  public function getAllOsobyAkce() {
    return $this->connection->table( self::AKCE_TABLE );
  }
  
  public function insertOsobaTypAkce($osoba_id, $akceTypId) {
    $akce = $this->getAllOsobyAkce()
                  ->where('kontaktni_osoba_id = ?', $osoba_id)
                  ->where('akce_typ = ?', $akceTypId);
    if ($akce->count() == 0) {
      $this->connection->query('INSERT INTO ' . self::AKCE_TABLE 
                              . '(kontaktni_osoba_id, akce_typ) VALUES '
                              . '(' . $osoba_id . ',' . $akceTypId . ')');
    }
  }
	
  public function deleteOsobaTypAkce($osoba_id, $akceTypId) {
    $akce = $this->connection->table( self:: AKCE_TABLE)
                             ->where('kontaktni_osoba_id = ?', $osoba_id)
                             ->where('akce_typ = ?', $akceTypId);
    if ($akce->count() != 0) {
      $akce->delete();
    }  
  }
	
  /**
   * @param array $osoba  
   * @return Nette\Database\Table\ActiveRow
   */     
  public function insertOsoba($osoba) {
    $result = null;
    
    $akceOsoby = array();
    if( isset($osoba['akce_typ'])
        && is_array($osoba['akce_typ'])
        && !empty($osoba['akce_typ']))
    {
      $akceOsoby = $osoba['akce_typ'];
    }
    unset($osoba['akce_typ']);      
    
    $result = $this->getTable()->insert($osoba);

    foreach($akceOsoby as $akce) {
      $this->insertOsobaTypAkce($result->kontaktni_osoba_id, $akce);
    }
    
    return $result;    
  }    
  
  /**
   * @param array $osoba  
   * @return Nette\Database\Table\ActiveRow
   */     
  public function updateOsoba($values) {
    $id = $values['kontaktni_osoba_id'];
    unset($values['kontaktni_osoba_id']);
    $osoba = $this->findById($id);
    
    $akceOsobyNew = array();
    if( isset($values['akce_typ'])
        && is_array($values['akce_typ'])
        && !empty($values['akce_typ']))
    {
      $akceOsobyNew = $values['akce_typ'];
    }
    unset($values['akce_typ']);      
    
    $akceOsobyOld = $this->getOsobaAkce($osoba);
    foreach($akceOsobyOld as $akceOld) {
      if(!in_array($akceOld,$akceOsobyNew)) {
        $this->deleteOsobaTypAkce($id, $akceOld);
      }
    }
    
    foreach($akceOsobyNew as $akceNew) {
      if(!in_array($akceNew,$akceOsobyOld)) {
        $this->insertOsobaTypAkce($id, $akceNew);
      }
    }
        
    $osoba->update($values);
    return $osoba;    
  }    
  
  /**
   * @return array (typ_id => nazev)
   */     
	public function fetchPossibleTypes() {
		return $this->kontaktni_osoba_types;
	}
  
  /**
   * @return string
   */     
  public function getOsobaTypeNazev($kontaktni_osoba_typ_id) {
    if (isset($this->kontaktni_osoba_types[$kontaktni_osoba_typ_id]))
      return $this->kontaktni_osoba_types[$kontaktni_osoba_typ_id];
    else return 'Nedefinovaný typ osoby';
  }
  
  /**
   * @return Nette\Database\Table\Selection
   */     
  public function getAkceByOsobaId($id) {
    return $this->getAllOsobyAkce()
                ->select('akce_typ')
                ->where('kontaktni_osoba_id = ?', $id);
  }                                                   

  /**
   * @param Nette\Database\Table\Selection $osoby
   * @return array(osoba_id => array)
   */     
  public function getOsobyAkce($osoby) {
    $result = array();
    foreach($osoby AS $osoba) {
      $result[$osoba->kontaktni_osoba_id] = $this->getOsobaAkce($osoba);
    }
    return $result;
  }

  /**
   * @param Nette\Database\Table\ActiveRow $osoba
   * @return array
   */     
  public function getOsobaAkce($osoba) {
    $result = array();
    foreach($osoba->related('kontaktni_osoba_akce') as $osobaAkce) {        
      $result[] = $osobaAkce->akce_typ;
    }
    return $result;
  }

  /**
   * Najde osoby dane skoly podle typu osoby
   * @author Petr Vitek   
   * @param int $skolaID
   * @param array $typy
   * @return \Nette\Database\Table\Selection
   */
  public function getContactsByTypes( $skolaID, $typy) {
    return $this->getTable()->select( 'kontaktni_osoba.*')
              ->where( 'kontaktni_osoba.skola_id = ?', $skolaID)
              ->where( 'kontaktni_osoba.kontaktni_osoba_typ_id', $typy);
  }
  
  /**
   * Najde osoby dane skoly podle typu akce
   * @author Petr Vitek   
   * @param int $skolaID
   * @param array $typyAkce 
   * @return \Nette\Database\Table\Selection
   */
  public function getContactsByAkce( $skolaID, $typyAkce) {
    return $this->getTable()->select( 'kontaktni_osoba.*')
              ->where( 'kontaktni_osoba.skola_id = ?', $skolaID)
              ->where( ':kontaktni_osoba_akce.akce_typ', $typyAkce);
  }
  
}
