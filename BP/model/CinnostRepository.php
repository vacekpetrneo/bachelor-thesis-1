<?php

namespace SCG;

use Nette;
use Nette\Diagnostics\Debugger;

class CinnostRepository extends Repository {

  /**
   * @return Nette\Database\Table\Selection
   */     
  public function findAll() {
    return $this->connection->table("cinnost")
                            ->select("cinnost.*")
                            ->select("skupina.nazev AS skupina")
                            ->select("akce.nazev AS akce")
                            ->select("akce.bezi AS akce_bezi")
                            ->select("uzivatel.login AS uzivatel")                            
                            ->order('datum DESC, cinnost_id DESC');                      
  }

	/**
	 * @param  array(int) | int $id
	 * @return Nette\Database\Table\Selection | Nette\Database\Table\ActiveRow
	 */
  public function findById($id) {
    $result = $this->findAll()->where("cinnost_id", $id);
    if (is_array($id)) {
      return $result;
    }
    else {
      return $result->fetch();
    }
  }

  public function findByUserId($userId) {
    return $this->findAll()->where("cinnost.uzivatel_id", $userId);
  }

  public function isVedouci($userId, $cinnostId) {
    $cinnost = $this->findById($cinnostId);
    $supp = $this->connection->table('opravneni')
                              ->where('opravneni.uzivatel_id', $userId)
                              ->where('opravneni.skupina_id', $cinnost->skupina_id)
                              ->where('opravneni.akce_id', $cinnost->akce_id)
                              ->count();
    return $supp > 0;                                    
  }
  
  public function findAsVedouciByUserId ($userId) {
    $opravneni = $this->connection->table('opravneni')
                                  ->select('akce_id, skupina_id')
                                  ->where('uzivatel_id', $userId);
    $clause = '(';
    foreach ($opravneni AS $supp) {
      $clause = $clause . '(' . $supp['akce_id'] . ',' . $supp['skupina_id'] . '),';     
    }
    $clause[strlen($clause)-1] = ')';
        
    $select = $this->findAll()->where('(cinnost.akce_id, cinnost.skupina_id) IN ' . $clause);
    return $select;
  }
  
}