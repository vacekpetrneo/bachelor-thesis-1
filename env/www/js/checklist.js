
function checklist_sendStavChange( checklistButton) {
	$( checklistButton).attr( 'disabled', 'disabled');
	$( checklistButton).after( '<img id="loading_image" src="' + gl_base_url + 'images/spinner.gif" />')
	checklistButton.oldVal = $(checklistButton).attr( 'value');
	var button = $( checklistButton).attr('id');
	var ids = button.split( '_');
	
	$.ajax( {
		url : gl_base_url + "checklist/stav/",
		cache : false,
		global : false,
		type: "POST",
		
		data: ({ turnaj_id : ids[1],
			  checklist_id : ids[2],
			  stav_id : ids[3],
			  stav_value : $(checklistButton).attr( 'value') } ),
		success: function( data, textStatus, XMLHttpRequest) { 
			  checklist_getResult( data, textStatus, XMLHttpRequest, checklistButton);
		  },
		error: function( XMLHttpRequest, textStatus, errorThrown) { 
			  checklist_errorResult( errorThrown, textStatus, XMLHttpRequest, checklistButton);
		  }
	});
		
}

function checklist_getResult(data, textStatus, XMLHttpRequest, checklistButton) {
	var newval;
	if ( checklistButton.oldVal == '1' ) {
		// marked as done
		newval = '0';
		$( checklistButton).parent().parent().addClass( 'checklist_done').removeClass( 'checklist_notdone');
	} else {
		// marked as not done
		newval = '1';
		$( checklistButton).parent().parent().removeClass( 'checklist_done').addClass( 'checklist_notdone');
	}
	$( checklistButton).removeAttr( 'disabled').attr( 'value', newval );
	$( '#loading_image').remove();
	checklist_checkComplete();
}

function checklist_errorResult( errorThrown, textStatus, XMLHttpRequest, checklistButton) {
	$( checklistButton).removeAttr( 'disabled');
	if ( checklistButton.oldVal == '1' ) {
		// was not marked, remove checked status if present
		$( checklistButton).removeAttr( 'checked');
	} else {
		// was marked, add checked back
		$( checklistButton).attr( 'checked', 'checked');
	}
	$( '#loading_image').remove();
}

function checklist_checkComplete() {
	$(".checklist_header").each( checklist_CheckPrefixTasks);
}

function checklist_CheckPrefixTasks() {
	prefix = this.id.split( '_')[1];
	alldone = true;
	elements = $( ".pre_" + prefix );
	if ( elements.hasClass( 'checklist_notdone') ) {
		alldone = false;
	}
	
	if ( alldone ) {
		$( this).addClass( 'checklist_done').removeClass( 'checklist_notdone');
	} else {
		$( this).addClass( 'checklist_notdone').removeClass( 'checklist_done');
	}
	
	done = elements.filter( ".checklist_done").size();
	 
	$( this).find( 'td:last').text( done + " / " + ( elements.size() ));
}

function checklist_hideItems() {
	$(".checklist_header").each( checklist_hideItem);
}

function checklist_toggleItems( headerElement ) {
	prefix = headerElement.id.split( '_')[1];
	$( ".pre_" + prefix ).toggle();
}

function checklist_hideItem( prefix) {
	prefix = this.id.split( '_')[1];
	$( ".pre_" + prefix ).hide();
}

