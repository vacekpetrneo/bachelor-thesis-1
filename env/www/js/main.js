$(function(){	
	$(".sortableTable").tablesorter();
});
$("input.datum").datepicker({
  showButtonPanel: true, dateFormat: "yy-mm-dd",
});
$("input.datumCas").datetimepicker({
  showButtonPanel: true,
  dateFormat: "yy-mm-dd",
  timeFormat: "HH:mm:ss",
  hourGrid: 6,
  minuteGrid: 10,
  stepMinute: 5,  
});
$("select[multiple]").multiSelectManipulator();
		