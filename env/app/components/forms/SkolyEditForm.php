<?php

namespace SCG;

use \Nette\Forms\Form;

/**
 * Formular pro editaci skoly
 *
 * @author rattus
 */
class SkolyEditForm extends \Nette\Application\UI\Form {
	
	public function __construct( array $Okresy, array $TypySkol, Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		parent::__construct( $parent, $name);
		
		$this->initForm( $Okresy, $TypySkol);
	}
	
	protected function initForm( array $Okresy, array $TypySkol ) {
    
    $this->getElementPrototype()->class = 'pure-form';
    
		$this->addProtection('Vypršel časový limit, odešlete prosím formulář znovu.', 300);
		
		$this->addText('nazev', "Název školy", 90, 255 )
				->addRule( \Nette\Forms\Form::MIN_LENGTH, "%label musí mít více jak 3 znaky", 3 )
				->addRule( \Nette\Forms\Form::MAX_LENGTH, "%label nesmí mít více jak 255 znaků", 255 )
				// FIXME : pridat check znaku
				// ->addRule( \SCG\Validate\CzechName::validate, "Musi obsahovat pouze ceske znaky")
				->setRequired();
		
		$this->addSelect( 'okres_id', "Okres" )
				->setItems( $Okresy)
				->setRequired();
				// ->addRule( \Nette\Forms\Form::INTEGER );
		
		
		$this->addMultiSelect( 'skola_typ_id', "Typ školy")
				->setItems( $TypySkol );
				// ->addRule( \Nette\Forms\Form::INTEGER );

		$this->addText( 'mesto', "Město")
				->addRule( \Nette\Forms\Form::MIN_LENGTH, "%label musí mít více jak 3 znaky", 3 )
				->addRule( \Nette\Forms\Form::MAX_LENGTH, "%label nesmí mít více jak 255 znaků", 255 )
				// FIXME : pridat check znaku
				// ->addRule( \SCG\Validate\CzechName::validate, "Musi obsahovat pouze ceske znaky")
				->setRequired();

		$this->addText( 'ulice', "Ulice")
				->addRule( \Nette\Forms\Form::MIN_LENGTH, "%label musí mít více jak 3 znaky", 3 )
				->addRule( \Nette\Forms\Form::MAX_LENGTH, "%label nesmí mít více jak 255 znaků", 255 )
				// FIXME : pridat check znaku
				// ->addRule( \SCG\Validate\CzechName::validate, "Musi obsahovat pouze ceske znaky")
				->setRequired();

		$this->addText( 'psc', "PSČ")
				->addRule( \Nette\Forms\Form::MIN_LENGTH, "%label musí mít více jak 3 znaky", 3 )
				->addRule( \Nette\Forms\Form::MAX_LENGTH, "%label nesmí mít více jak 20 znaků", 20 )
				->addRule( \Nette\Forms\Form::PATTERN, "%label musí obsahovat pouze čísla a písmena", "^[a-z0-9 ]*$" )
				// FIXME : pridat check znaku
				// ->addRule( \SCG\Validate\CzechName::validate, "Musi obsahovat pouze ceske znaky")
				->setRequired();

		$this->addText( 'email', "Email")
				->addCondition( Form::FILLED )
					->addRule( \Nette\Forms\Form::MIN_LENGTH, "%label musí mít více jak 3 znaky", 3 )
					->addRule( \Nette\Forms\Form::MAX_LENGTH, "%label nesmí mít více jak 255 znaků", 255 )
					->addRule( \Nette\Forms\Form::EMAIL, "%label musí být platná email adresa" );
		
		$this->addText( 'web', "Web")
				->addCondition( Form::FILLED )
					->addRule( \Nette\Forms\Form::MIN_LENGTH, "%label musí mít více jak 6 znaků", 6 )
					->addRule( \Nette\Forms\Form::MAX_LENGTH, "%label nesmí mít více jak 255 znaků", 255 )
					->addRule( \Nette\Forms\Form::PATTERN, "%label musí být platná url adresa", "^[a-z0-9+ -:\/\.\?&#%]*$" );
		
		$this->addText( 'telefon', "Telefon")
				->addCondition( Form::FILLED )
					->addRule( \Nette\Forms\Form::MIN_LENGTH, "%label musí mít více jak 6 znaků", 6 )
					->addRule( \Nette\Forms\Form::MAX_LENGTH, "%label nesmí mít více jak 255 znaků", 255 )
					->addRule( \Nette\Forms\Form::PATTERN, "%label musí být telefonní číslo", "^[a-z0-9+ -]*$" );
		
		$this->addText( 'ico', "IČO")
				->addCondition( Form::FILLED )
					->addRule( \Nette\Forms\Form::MIN_LENGTH, "%label musí mít více jak 1 znak", 1 )
					->addRule( \Nette\Forms\Form::MAX_LENGTH, "%label nesmí mít více jak 40 znaků", 40 )
					->addRule( \Nette\Forms\Form::PATTERN, "%label musí být telefonní číslo", "^[a-z0-9\- ]*$" );

		$this->addText( 'pocet_zaku', "Počet žáků")
				->addCondition( Form::FILLED )
					->addRule( \Nette\Forms\Form::INTEGER, "%label musí být číslo");
		
		$this->addTextArea('poznamka', "Poznámka", 60, 10)
				// FIXME : pridat check znaku
				// ->addRule( \SCG\Validate\CzechText::validate, "Musi obsahovat pouze ceske znaky")
				;
		
		$this->addHidden( 'skola_id' );

    $this->setCurrentGroup();
		$this->addSubmit( 'ulozit', 'Uložit změny')->setAttribute('class', 'pure-button pure-button-primary');;
	}
	
	public function render() {
		
		$args = func_get_args();
//		\Nette\Diagnostics\Debugger::barDump( $args, "SkolyEditForm::render" );
		$skola = array_shift( $args );
		if ( $skola instanceof \Traversable || $skola instanceof \Iterator || is_array( $skola ) ) {
			
			$typy = array_shift( $args );
			
			$skola = $skola->toArray();
			$skola['skola_typ_id'] = $typy[$skola['skola_id']];
			
			$this->setValues( $skola );
			$this['skola_id']->value = $skola['skola_id'];
			
		} else {
			array_unshift( $args, $skola );
		}
		
		call_user_func_array(array( 'parent', 'render'), $args);
	}
	
}
