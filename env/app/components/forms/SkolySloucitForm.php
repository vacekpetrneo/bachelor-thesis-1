<?php

namespace SCG;

/**
 * Formular pro slucovani skol
 *
 * @author rattus
 */
class SkolySloucitForm extends SkolyEditForm {
	
	// FIXME : misto dedeni formu pouzit service tovarny 
	private $primarniGroup = null;
	
	public function __construct( array $Okresy, array $TypySkol, Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		parent::__construct( $Okresy, $TypySkol, $parent, $name);
		
	}
	
	protected function initForm( array $Okresy, array $TypySkol ) {
		$this->primarniGroup = $this->addGroup( "Primární škola");
		$this->addSelect( 'primarni_skola', "Primární škola");
		$this->addHidden( 'skoly_ids');
		$this->addGroup( "Data sloučené školy");
		parent::initForm( $Okresy, $TypySkol );
	}
	
	public function render() {
		
		$args = func_get_args();
		$skoly = array_shift( $args );
//		\Nette\Diagnostics\Debugger::barDump( $args, "SkolySloucitForm::render" );
		if ( $skoly instanceof \Iterator ) {
			$skolyIDs = array();
			foreach( $skoly as $skola ) {
				$skolyIDs[$skola->skola_id] = $skola->nazev;
			}

			$originalGroup = $this->getCurrentGroup();
			$this->setCurrentGroup( $this->primarniGroup );
			$this['primarni_skola']->setItems( $skolyIDs );
			$this['skoly_ids']->value = implode( ',', array_keys( $skolyIDs ) );
			$this->setCurrentGroup( $originalGroup );
			
			$skoly->rewind();
			$skola = $skoly->current();
			
			array_unshift( $args, $skola );
		} else {
			array_unshift( $args, $skoly );
		}
		// call parent with same arguments
		call_user_func_array(array( 'parent', 'render'), $args);
	}
	
}
