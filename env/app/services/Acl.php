<?php

namespace SCG;

use Nette, Nette\Security;

/**
 * Permission loaded from AclModel - db
 *
 */
class Acl extends Nette\Security\Permission {
    
    private $initDone = false;
    
    /** @var AclModel */
    private $model;
    
    public function __construct( Nette\Database\Context $connection ) {
        $this->model = new AclModel( $connection );
    }
    
    /**
	 * Sets cache storage engine
	 * @param  Nette\Caching\IStorage $storage
	 * @return Acl provides a fluent interface
	 */
    public function setCacheStorage(Nette\Caching\IStorage $storage = NULL)
    {
      $this->model->setCacheStorage( $storage );
      return $this;
    }
    
    private function initData() {
        if ( $this->initDone === false ) {
			$this->addRole( 'authenticated' );
			$this->addRole( 'guest' );
            foreach($this->model->getRoles() as $role)
                $this->addRole( $role['name'], $role['parent_name'] );

            foreach($this->model->getResources() as $resource)
                $this->addResource($resource['name']);

            foreach($this->model->getRules() as $rule)
                $this->{$rule['allowed'] == 1 ? 'allow' : 'deny'}( $rule['role'], $rule['resource'], $rule['privilege'] );

            $this->initDone = true;
        }
    }
    
    /**
	 * Returns TRUE if and only if the Role has access to [certain $privileges upon] the Resource.
	 *
	 * This method checks Role inheritance using a depth-first traversal of the Role list.
	 * The highest priority parent (i.e., the parent most recently added) is checked first,
	 * and its respective parents are checked similarly before the lower-priority parents of
	 * the Role are checked.
	 *
	 * @param  string|Permission::ALL|IRole  role
	 * @param  string|Permission::ALL|IResource  resource
	 * @param  string|Permission::ALL  privilege
	 * @throws InvalidStateException
	 * @return bool
	 */
    public function isAllowed($role = self::ALL, $resource = self::ALL, $privilege = self::ALL) {
           $this->initData();
           return parent::isAllowed( $role, $resource, $privilege );
    }
    
    public function getRoles() {
        return $this->model->getRoles();
    }
	
	public function getPrivileges() {
		return $this->model->getPrivileges();
	}
        
}
