<?php

namespace SCG;

use Nette, Nette\Security;

/**
 * Description of SCGUser
 *
 * @author rattus
 */
class User extends Nette\Security\User {
	
	/**
	 * Pretizena metoda z Nette\Security\User, zakazana, vzdy vrati false. pouzit isAllowedForAkce kde se zpracuje i akce
	 * @param IResource $resource
	 * @param string $privilege
	 * @return FALSE
	 */
	public function isAllowed($resource = IAuthorizator::ALL, $privilege = IAuthorizator::ALL) {
		trigger_error( 'This method is disabled, use isAllowedForAkce/isAllowedForSomeAkce instead', E_USER_WARNING);
		return FALSE;
	}
	
	/**
	 * Vrati role uzivatele pro danou akci
	 * @param int $AkceID
	 * @return array
	 */
	public function getRolesForAkce( $AkceID ) {
		if (!$this->isLoggedIn()) {
			return array($this->guestRole);
		}

		$AkceID = (int) $AkceID;
		$identity = $this->getIdentity();
		if ( $identity ) {
			$akceRoles = $identity->getRoles();
			if ( isset( $akceRoles[ $AkceID]) ) {
				$akceRoles[ $AkceID][] = $this->authenticatedRole;
				return $akceRoles[ $AkceID];
			}
		}
		return array($this->authenticatedRole);
	}
	
	/**
	 * Vrati vsechny role ze vsech akci uzivatele
	 */
	public function getRolesForAllAkce() {
		if (!$this->isLoggedIn()) {
			return array($this->guestRole);
		}

		$identity = $this->getIdentity();
		if ( $identity ) {
			$allRoles = array( $this->authenticatedRole => true);
			$akceRoles = $identity->getRoles();
			foreach( $akceRoles as $roles ) {
				foreach( $roles as $role ) {
					$allRoles[ $role] = true;
				}
			}
			return array_keys( $allRoles);
		}
		return array($this->authenticatedRole);
	}
	
	/**
	 * Zkontroluje zda uzivatel ma opravneni alespon v jedne akci
	 * @param IResource $resource
	 * @param string $privilege
	 * @return boolean
	 */
	public function isAllowedForSomeAkce( $resource = IAuthorizator::ALL, $privilege = IAuthorizator::ALL ) {
		$authorizator = $this->getAuthorizator();
		foreach ( $this->getRolesForAllAkce() as $role) {
			if ( $authorizator->isAllowed( $role, $resource, $privilege) ) {
				return TRUE;
			}
		}

		return FALSE;
	}
	
	/**
	 * Zkontroluje opravneni uzivatele v dane akci
	 * @param int $AkceID
	 * @param IResource $resource
	 * @param string $privilege
	 * @return boolean
	 */
	public function isAllowedForAkce( $AkceID, $resource = IAuthorizator::ALL, $privilege = IAuthorizator::ALL ) {
		// get role for akce
		$authorizator = $this->getAuthorizator();
		foreach ( $this->getRolesForAkce( $AkceID ) as $role) {
			if ( $authorizator->isAllowed( $role, $resource, $privilege) ) {
				return TRUE;
			}
		}

		return FALSE;
	}
	
	public function getAllowedAkceForPrivilege( $Privilege ) {
		// FIXME : vrati akce_id pro akce kde ma uzivatel povoleno opravneni privilege
		$authorizator = $this->getAuthorizator();
		$privileges = array();
		foreach( $authorizator->getRoles() as $role ) {
			foreach ( $this->getRolesForAkce( $AkceID ) as $role) {
				if ( $authorizator->isAllowed( $role, IAuthorizator::ALL, $Privilege ) ) {
					$privileges[] = $privilege;
				}
			}
		}
		return $privileges;
	}
	
}
