<?php

namespace SCG;

use \Nette\Diagnostics\Logger;

class MassMailer {

  const MAIL_RECIPIENTS_LIMIT = 15;
  const MAIL_BATCH_LIMIT = 4;
  const STATUS_ERROR = 'error';
  const STATUS_WAIT_APPROVE = 'wappr';
  const STATUS_WAIT_PUBLISH = 'wdate';
  const STATUS_WAIT_TEST = 'wtest';

  /** @var string Path to directory with mails */
  protected $_DataPath = '';

  /** @var string Path to directory with mail preparation zips */
  protected $_MailPreparePath = '';

  /** @var string File to store mail logs */
  protected $_LogFile = '';

  /** @var \Nette\Diagnostics\Logger */
  protected $_Logger = null;

  /**
   * 
   * @param string $dataPath Path to files with mail xml
   */
  public function __construct($dataPath, $mailPreparePath, $logFilePath) {
    $this->_DataPath = $dataPath;
    $this->_MailPreparePath = $mailPreparePath;
    $this->_LogFile = $logFilePath;
  }

  /**
   * Vrati cestu na ftp kde jsou zipy s pripravenymi prilohami
   * @return string
   */
  public function getPreparedMailPath() {
    return $this->_MailPreparePath;
  }
  
  public function addSouteziciJob($data, User $user) {
    throw new Exception("Not implemented yet");

    if (!isset($data['tymy']) || !is_array($data['tymy']) || count($data['tymy']) == 0) {
      throw new Exception("Nejsou udány žádné cílové týmy");
    }

    $_XMLSchema = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><mail></mail>');

    $_subject = $_XMLSchema->addChild('subject', strip_tags($data['subject']));
    $_body = $_XMLSchema->addChild('body', htmlspecialchars($data['body'], ENT_QUOTES, 'UTF-8'));
    $_recipients = $_XMLSchema->addChild('recipients');

    if (isset($data['attachement']) && $data['attachement']['error'] == 0) {
      $_attachFileName = $this->_DataPath . $date . $data['hash'] . '-file';
      if (move_uploaded_file($data['attachement']['tmp_name'], $_attachFileName)) {

        $_attachement = $_XMLSchema->addChild('attachement');
        $_attachement->addChild('file', $_attachFileName);
        $_attachement->addChild('name', $data['attachement']['name']);
        $_attachement->addChild('size', $data['attachement']['size']);
        $_attachement->addChild('type', $data['attachement']['type']);
        @chmod($_attachFileName, 0660);
      } else {
        throw new Exception("Chyba ukládání přílohy do souboru '{$_attachFileName}'");
      }
    }

    $userIdent = null;
    if ($user->getIdentity() instanceof \Nette\Security\Identity) {
      $userIdent = $user->getIdentity();
      $_userXML = $_XMLSchema->addChild('user');
      $_userXML->addChild('login', $userIdent->login);
      $_userXML->addChild('name', $userIdent->krestni . ' ' . $userIdent->prijmeni);
      $_userXML->addChild('email', $userIdent->mail);
    }

    if (isset($data['test_mail_address'])) {
      $_XMLSchema->addChild("test_mail_address", $data['test_mail_address']);
    } else if ($userIdent != null) {
      $_XMLSchema->addChild("test_mail_address", $userIdent->mail);
    }

    $_to = $_recipients->addChild('to');
    $_jobNumber = 0;

    #  pro kazdy tym samostatny mail
    foreach ($data['tymy'] as $_tymEmails) {
      $emails = array(); # pouzite maily pro tym - kontrola duplicit

      foreach ($_tymEmails as $_email) {
        $_email = trim($_email);
        if (!isset($emails[$_email])) {
          // preskocit duplicity
          $_to->addChild('osoba', $_email);
          $emails[$_email] = true;
        }
      }

      # uloz jako cast mailu, vynuluj BCC a pokracuj
      $this->_saveXMLJob($data['hash'], $_XMLSchema, $_jobNumber++);
      unset($_recipients->to);
      $_to = $_recipients->addChild('to');
    }

//        $this->_saveXMLJob( $data['hash'], $_XMLSchema, $_jobNumber);

    SCG_History::logAction('mail', 'massmail', count($data['tymy']));

    return TRUE;
  }

  public function addSkolyJob($data, \Nette\Security\Identity $user, SkolaRepository $skolaRepo, OsobaRepository $osobaRepo ) {
    
    if (!isset($data['skoly']) || !is_array($data['skoly']) || count($data['skoly']) == 0) {
      throw new Exception("Nejsou udány žádné cílové školy");
    }

    $hash = $data['hash'] . '-' . $data['prefix'];
    
    $_XMLSchema = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><mail></mail>');

    if (isset($data['cas']) && trim($data['cas']) != '') {
      $timestamp = strtotime(str_replace('.', '-', $data['cas']));
      if (date('Y.m.d H:i:s', $timestamp) != $data['cas'] &&
              date('Y.m.d H:i', $timestamp) != $data['cas'] &&
              date('Y.m.d H', $timestamp) != $data['cas']) {
        throw new Exception("Datum nebylo rozpoznáno - {$data['cas']} -> " . date('Y.m.d H:i:s', $timestamp));
      }
      $_XMLSchema->addChild('publish_timestamp', $timestamp);
    }

    $_subject = $_XMLSchema->addChild('subject', strip_tags($data['subject']));
    $_body = $_XMLSchema->addChild('body', htmlspecialchars($data['body'], ENT_QUOTES, 'UTF-8'));
    $_recipients = $_XMLSchema->addChild('recipients');

    $finfo = new \finfo( FILEINFO_MIME_TYPE );
    
    $attach_count = 0;
    if (isset($data['attachements'])) {
      $date = date('Y.m.d_');
      foreach ($data['attachements'] as $attachement) {
          ++$attach_count;
          $_attachFileName = $this->_DataPath . $date . $hash . '-file-' . $attach_count;
          
          if (copy($attachement, $_attachFileName)) {

            $_attachement = $_XMLSchema->addChild('attachement');
            $_attachement->addChild('file', $_attachFileName);
            // TODO : overit ze priloha je jen jedna a cislovat prilohy
            // $_attachement->addChild('name', basename( $attachement));
            $_attachement->addChild('name', 'PPT2014-pozvanka.pdf');
            // $_attachement->addChild('size', $attachement['size']);
            $_attachement->addChild('type', $finfo->file( $attachement) );
            @chmod($_attachFileName, 0660);
          } else {
            throw new Exception("Chyba ukládání přílohy do souboru '{$_attachFileName}'");
          }
      }
    }

    if (isset($data['from_mail']) && !empty($data['from_mail'])) {
      $_from = $_XMLSchema->addChild('from');
      $_from->addChild('mail', $data['from_mail'] . '@scg.cz');
      $_from->addChild('label', $data['from_label']);
    }

      $_userXML = $_XMLSchema->addChild('user');
      $_userXML->addChild('login', $user->login);
      $_userXML->addChild('name', $user->krestni . ' ' . $user->prijmeni);
      $_userXML->addChild('email', $user->mail);
    
    if (isset($data['test_mail_address'])) {
      $_XMLSchema->addChild("test_mail_address", $data['test_mail_address']);
    } else if ($user != null) {
      $_XMLSchema->addChild("test_mail_address", $user->mail);
    } else {
      throw new Exception("Chybí testovací adresa");
    }

    $_to = $_recipients->addChild('to');
    $_cc = $_recipients->addChild('cc');
    $_jobNumber = 0;

    #  pro kazdou skolu samostatny mail
    foreach ($data['skoly'] as $_skolaID) {
      $emails = array(); # pouzite maily pro skolu - kontrola duplicit
      $skola = $skolaRepo->findById( (int) $_skolaID );
      if ( $skola === false ) {
        // throw new \Exception("Nenalezena skola id {$_skolaID} - prosim zkuste to znovu");
        $this->_Logger->log( "Nenalezena skola id {$_skolaID} z turnaje {$data['prefix']}, oveřte prosím. Přeskočena.", Logger::ERROR );
        continue;
      }
      
      // \Nette\Diagnostics\Debugger::barDump( $data, "Posilani skole {$skola->skola_id} : {$skola->ulice}");
      
      if ($data['also_main'] == true && trim($skola->email) != '') {
        # pokud posilame na sekretariat
        $_skolaEl = $_to->addChild('skola', trim($skola->email));
        $_skolaEl->addAttribute('id', $skola->skola_id);
        $emails[trim($skola->email)] = true;
      }

      if ( isset( $data['also_primary'] ) && $data['also_primary'] == true) {
        $kontakty = $skolaRepo->getPrimaryContacts( $skola );

        foreach ($kontakty as $osoba) {
          # ma mail a jeste nebyl pouzit
          if (trim($osoba->email) != '' && !isset($emails[trim($osoba->email)])) {
            # primarnim kontaktum posleme taky
            $_cc->addChild('osoba', trim($osoba->email));
            $emails[trim($osoba->email)] = true;
          }
        }
      }

      if (isset($data['kontakttyp']) && is_array($data['kontakttyp']) && count($data['kontakttyp']) > 0) {
        $kontakty = $osobaRepo->getContactsByTypes( $skola->skola_id, $data['kontakttyp']);

        foreach ($kontakty as $osoba) {
          # ma mail a jeste nebyl pouzit
          if ($osoba->primarni && trim($osoba->email) != '' && !isset($emails[trim($osoba->email)])) {
            # primarnim kontaktum posleme taky
            $_cc->addChild('osoba', trim($osoba->email));
            $emails[trim($osoba->email)] = true;
          }
        }
      }

      if (isset($data['kontaktakce']) && is_array($data['kontaktakce']) && count($data['kontaktakce']) > 0) {
        $kontakty = $osobaRepo->getContactsByAkce( $skola->skola_id, $data['kontaktakce']);

        foreach ($kontakty as $osoba) {
          // \Nette\Diagnostics\Debugger::barDump( $osoba, "Posilani skole {$skola->skola_id} : {$skola->ulice}");
          # ma mail a jeste nebyl pouzit
          if ($osoba->primarni && trim($osoba->email) != '' && !isset($emails[trim($osoba->email)])) {
            # primarnim kontaktum posleme taky
            // \Nette\Diagnostics\Debugger::barDump( "Pridavame {$osoba->email}", "Posilani skole {$skola->skola_id} : {$skola->ulice}");
            $_cc->addChild('osoba', trim($osoba->email));
            $emails[trim($osoba->email)] = true;
          }
        }
      }

      // vsechny kontaktni osoby co zapsaly tym do jedne z vybranych akci
      if (isset($data['vybrane_akce']) && is_array($data['vybrane_akce']) && count($data['vybrane_akce']) > 0) {
        $kontakty = $skolaRepo->getOsobyInAkce( $skola, $data['vybrane_akce']);

        foreach ($kontakty as $osoba) {
          # ma mail a jeste nebyl pouzit
          if (trim($osoba->email) != '' && !isset($emails[trim($osoba->email)])) {
            # kontaktum z vybranych akci
            $_cc->addChild('osoba', trim($osoba->email));
            $emails[trim($osoba->email)] = true;
          }
        }
      }

      // \Nette\Diagnostics\Debugger::barDump( $_XMLSchema, "XML skole {$skola->skola_id} : {$skola->ulice}");
      // \Nette\Diagnostics\Debugger::barDump( $emails, "Emaily skole {$skola->skola_id} : {$skola->ulice}");
      
      if (count($emails) > 0) {
        # uloz jako cast mailu, vynuluj BCC a pokracuj
        $this->_saveXMLJob( $hash , $_XMLSchema, $_jobNumber++);
      } else {
        if ($this->_Logger != null)
          $this->_Logger->log("Škola '{$skola->nazev}' {$skola->mesto} ({$skola->skola_id}) neměla vybrané žádné adresáty", Logger::WARNING);
      }

      unset($_recipients->cc);
      unset($_recipients->to);
      $_to = $_recipients->addChild('to');
      $_cc = $_recipients->addChild('cc');
    }

//        $this->_saveXMLJob( $data['hash'], $_XMLSchema, $_jobNumber);
    // FIXME : scg history
    // SCG_History::logAction('mail', 'massmail', count($data['skoly']));

    return TRUE;
  }

  public function processJobs() {
    throw new Exception("Not implemented yet");

    $path = $this->_DataPath;

    $this->_Logger = new Logger();
    $this->_Logger->directory = $this->_LogFile;

    $mailsProcessed = 0;
    $directory = dir($path);
    $mailingCycles = 0;
    while (( $entry = $directory->read() ) !== FALSE) {

      //po kazdych ctyrech mailech na minutu usni, opakovat 55 minut (cron spousti skript kazdou hodinu)
      if ($mailsProcessed >= self::MAIL_BATCH_LIMIT) {
        $mailsProcessed = 0;
        $mailingCycles += 1;
        if ($mailingCycles > 55)
          break;
        sleep(60);
      }

      if (substr($entry, -4) == '.xml') {
        try {
          $mailsProcessed += $this->_sendMail($path . '/' . $entry, false);
        } catch (Exception $e) {
          // TODO : zapsat chybu do xml souboru aby se dala zobrazit v prehledu
          $this->_Logger->log($path . '/' . $entry . ' - error ' . $e->getMessage(), Logger::CRITICAL);
        }
      }
    }

    $directory->close();
  }

  /**
   * Ulozi data o mailu do xml souboru
   * @param string $hash unikatni hash mailu
   * @param SimpleXMLElement $XMLData XML strom mailu
   * @param int $JobNumber Poradove cislo mailoveho souboru ( pro kazdou cilovou skolu se tvori jeden )
   * @throws Exception Chyba ulozeni 
   */
  protected function _saveXMLJob($hash, $XMLData, $JobNumber = 0) {

    $date = date('Y.m.d_');
    $number = ( $JobNumber != 0) ? '-' . $JobNumber : '';
    # pokud neni nula ,pridej do jmena souboru poradove cislo
    $filename = $this->_DataPath . $date . $hash . '-mail' . $number . '.xml';

    // \Nette\Diagnostics\Debugger::barDump( "Ukladame {$filename}");
    
    $file = @fopen($filename, 'w');

    if ($file === FALSE) {
      throw new Exception("Chyba ukládání mailu do '{$filename}'");
    }
    @fclose($file);

    $_doc = new \DOMDocument('1.0', 'utf-8');
    $_doc->formatOutput = true;
    $_domnode = dom_import_simplexml($XMLData);
    $_domnode = $_doc->importNode($_domnode, true);
    $_domnode = $_doc->appendChild($_domnode);

    $_doc->save($filename, LIBXML_NOEMPTYTAG);

    @chmod($filename, 0660);
  }

  /**
   * Provede pokus o odeslani mailu
   * @param string $filename Soubor mailu
   * @param boolean $testMail Zda se jedna o test mail, default false
   * @return int 0 pokud nebyl poslan, 1 pokud se poslal
   */
  protected function _sendMail($filename, $testMail = false) {
    throw new Exception("Not implemented yet");

    try {
      $_XMLSchema = @simplexml_load_file($filename);
    } catch (Exception $e) {
      $this->_Logger->log("Error opening/parsing file {$filename}", Logger::ERROR);
      return 0;
    }

    if ($_XMLSchema === FALSE) {
      $this->_Logger->log("Error opening/parsing file {$filename}", Logger::ERROR);
      return 0;
    }

    // neni test mail a ma publish time
    if (!$testMail &&
            isset($_XMLSchema->publish_timestamp)) {
      $publishTimestamp = (int) $_XMLSchema->publish_timestamp;
      if ($publishTimestamp > time()) {
        // not time yet
        return 0;
      }
    }

    // pokud neni testmail a zaroven neni approved, neposilat vubec
    if (!$testMail &&
            (!isset($_XMLSchema->approved) || ( (string) $_XMLSchema->approved ) != 'yes' )
    ) {
      return 0;
    }

    /* Purifier odebran z mailu
      require_once 'HTMLPurifier/HTMLPurifier.auto.php';
      # prebit font-family validaci
      require_once 'SCG/HTMLPurifier/AttrDef/CSS/FontFamily.php';
      $config = HTMLPurifier_Config::createDefault();
      $css = $config->getCSSDefinition();
      unset( $css->info['font-size']);
      unset( $css->info['line-height']);

      $purifier = new HTMLPurifier( $config);
     */
    // require_once 'Zend/Mail.php';
    $mail = new Zend_Mail('utf-8');

    $htmlBody = html_entity_decode((string) $_XMLSchema->body, ENT_QUOTES, 'UTF-8');

    // FIXME : odjebavka akcniho TinyMCE co nam kazi ctvrtecni vecery 
    $htmlBody = str_replace(array('<!--', '-->'), '', stripslashes($htmlBody));

    # TODO : pouzit standardni hlavicku pro vsechny SCG maily ?
    $footer = 'Za SCG ' . (string) $_XMLSchema->user->name .
            '<' . (string) $_XMLSchema->user->email . '>';

    $mail->setBodyText(strip_tags(html_entity_decode((string) $_XMLSchema->body, ENT_QUOTES, 'UTF-8')), 'utf-8', Zend_Mime::ENCODING_BASE64)
            // ->setBodyHtml( $purifier->purify( (string) $_XMLSchema->body) )
            ->setBodyHtml($htmlBody, 'utf-8', Zend_Mime::ENCODING_BASE64)
            ->setSubject(strip_tags((string) $_XMLSchema->subject));

    $xmlTarget = $_XMLSchema->recipients;

    $recipients = array('to' => array(), 'cc' => array(), 'bcc' => array());

    if ($testMail) {
      $mail->addTo((string) $_XMLSchema->test_mail_address);
    }

    # zpracuj TO
    $firstCCtoTo = false;
    $to = $xmlTarget->to->children();
    if (count($to) != 0) {
      foreach ($to as $target) {
        $recipients['to'][] = (string) $target;
        if (!$testMail) {
          $mail->addTo((string) $target);
        }
      }
    } else {
      $firstCCtoTo = true;
    }

    # zpracuj CC
    $cc = $xmlTarget->cc->children();
    if (count($cc) != 0) {
      foreach ($cc as $target) {
        if ($firstCCtoTo) {
          $recipients['to'][] = (string) $target;
          if (!$testMail) {
            $mail->addTo((string) $target);
          }
          $firstCCtoTo = false;
        } else {
          $recipients['cc'][] = (string) $target;
          if (!$testMail) {
            $mail->addCc((string) $target);
          }
        }
      }
    }

    # zpracuj BCC
    if (isset($xmlTarget->bcc)) {
      $bcc = $xmlTarget->bcc->children();
      if (count($bcc) != 0) {
        foreach ($bcc as $target) {
          $recipients['bcc'][] = (string) $target;
          if (!$testMail) {
            $mail->addBcc((string) $target);
          }
        }
      }
    }

    # zpracuj From
    if (isset($_XMLSchema->from)) {
      $mail->setFrom((string) $_XMLSchema->from->mail, (string) $_XMLSchema->from->label);
    } else {
      $mail->setFrom('info@scg.cz', 'SCG Info');
    }


    if (count($mail->getRecipients()) == 0) {
      # cile jsou prazdne: smazat
      $this->_addError($_XMLSchema, $filename, "Mail nemá žádný cíl");
      $this->_Logger->log("{$filename} nema zadny cil", Logger::ERROR);
      return 0;
    }

    if ($testMail) {
      // add recipients to the end of the mail
      $recText = "\nTo : " . implode(';', $recipients['to']) . "\n"
              . "CC : " . implode(';', $recipients['cc']) . "\n"
              . "BCC : " . implode(';', $recipients['bcc']) . "\n";

      $mail->setBodyText(base64_decode($mail->getBodyText(true)) . $recText, 'utf-8', Zend_Mime::ENCODING_BASE64);
      $recHtml = "<br><ul><li>To : " . implode(';', $recipients['to']) . "</li>\n"
              . "<li>CC : " . implode(';', $recipients['cc']) . "</li>\n"
              . "<li>BCC : " . implode(';', $recipients['bcc']) . "</li></ul>\n";

      $mail->setBodyHtml(base64_decode($mail->getBodyHtml(true)) . $recHtml, 'utf-8', Zend_Mime::ENCODING_BASE64);
    }

    if (isset($_XMLSchema->attachement)) {
      foreach ($_XMLSchema->attachement as $attachement) {
        $data = @file_get_contents((string) $attachement->file);
        if ($data === FALSE) {
          # chyba nacitani prilohy
          $this->_addError($_XMLSchema, $filename, "Chyba přidání přílohy");
          $this->_Logger->log("Chyba nacitani prilohy " . (string) $attachement->file
                  . " z mailu {$filename}", Logger::ERROR);
          return 0;
        }

        $mail->createAttachment($data, (string) $attachement->type, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, (string) $attachement->name);
      }
    }

    try {
      $this->processStyles($mail);
    } catch (Exception $e) {
      $this->_addError($_XMLSchema, $filename, $e->getMessage());
      $this->_Logger->log($filename . ' - error ' . $e->getMessage(), Logger::CRITICAL);
      return 0;
    }
    try {
      $mail->send();
    } catch (Zend_Mail_Exception $e) {
      $this->_addError($_XMLSchema, $filename, $e->getMessage());
      $this->_Logger->log($filename . ' - error ' . $e->getMessage(), Logger::CRITICAL);
      return 0;
    }

    # TODO : uchovavat kopie odeslanych ?
    if (!$testMail) {
      $recText = " To : " . implode(',', $recipients['to']) . ";"
              . "CC : " . implode(',', $recipients['cc']) . ";"
              . "BCC : " . implode(',', $recipients['bcc']) . ";";

      $this->_Logger->log("Odeslan mail {$filename} {$recText}", Logger::DEBUG);

      unlink($filename);
      if (file_exists($filename)) {
        $this->_addError($_XMLSchema, $filename, "Chyba mazání souboru");
        $this->_Logger->log($filename . ' - error : Chyba mazání souboru', Logger::CRITICAL);
      }
    } else {
      $recText = (string) $_XMLSchema->test_mail_address;
      $this->_Logger->log("Odeslan test mail {$filename} na {$recText}", Logger::DEBUG);

      $this->_setTestSent($filename);
    }

    return 1;
  }

  /**
   * Gets hash part of the filename
   * @param string $filename
   * @return string
   */
  protected static function _getHashFromFilename($filename) {
    $entry = basename($filename);
    $tmp = preg_split('/[-_]/', $entry);
    if (count($tmp) > 2) {
      return $tmp[1];
    }
    return $entry;
  }

  /**
   * Oznaci mail XML ze uspesne odeslal testovaci mail
   * @param SimpleXMLElement $XMLData
   * @param string $filename
   * @return boolean
   * @throws Exception Chyba zapisu do souboru
   */
  protected function _setTestSent($filename) {
    throw new Exception("Not implemented yet");

    $hash = self::_getHashFromFilename($filename);
    $files = self::_getFilesByHash($hash);

    if (!isset($files['mail']) ||
            !is_array($files['mail']) ||
            count($files['mail']) == 0) {
      return FALSE;
    }
    $modified = 0;
    foreach ($files['mail'] as $filename) {

      try {
        $_XMLSchema = @simplexml_load_file($filename);
      } catch (Exception $e) {
        $this->_Logger->log("Error opening/parsing file {$filename}", Logger::ERROR);
        return FALSE;
      }

      if ($_XMLSchema === FALSE) {
        $this->_Logger->log("Error opening/parsing file {$filename}", Logger::ERROR);
        return FALSE;
      }

      if (isset($_XMLSchema->test_sent)) {
        continue;
      } else {
        $_XMLSchema->addChild('test_sent', 'yes');
      }

      $file = @fopen($filename, 'w');
      if ($file === FALSE) {
        $this->_Logger->log("Chyba ukládání mailu do '{$filename}'", Logger::ERROR);
        return FALSE;
      }
      @fclose($file);

      $_doc = new DOMDocument('1.0', 'utf-8');
      $_doc->formatOutput = true;
      $_domnode = dom_import_simplexml($_XMLSchema);
      $_domnode = $_doc->importNode($_domnode, true);
      $_domnode = $_doc->appendChild($_domnode);

      $_doc->save($filename, LIBXML_NOEMPTYTAG);

      @chmod($filename, 0660);
      $modified++;
    }
    return $modified;
  }

  /**
   * Prida/nastavi error do souboru s mailem podle XML
   * @param SimpleXMLElement $XMLData XML mailu
   * @param string $filename cesta k souboru s mailem 
   * @param string $error Chybova hlaska
   * @throws Exception Pokud nejde otevrit cilovy soubor k zapisu
   */
  protected function _addError($XMLData, $filename, $error) {
    throw new Exception("Not implemented yet");

    if (isset($XMLData->error)) {
      $XMLData->error = htmlspecialchars($error, ENT_QUOTES, 'UTF-8');
    } else {
      $XMLData->addChild('error', htmlspecialchars($error, ENT_QUOTES, 'UTF-8'));
    }

    $file = @fopen($filename, 'w');
    if ($file === FALSE) {
      throw new Exception("Chyba ukládání mailu do '{$filename}'");
    }
    @fclose($file);

    $_doc = new DOMDocument('1.0', 'utf-8');
    $_doc->formatOutput = true;
    $_domnode = dom_import_simplexml($XMLData);
    $_domnode = $_doc->importNode($_domnode, true);
    $_domnode = $_doc->appendChild($_domnode);

    $_doc->save($filename, LIBXML_NOEMPTYTAG);

    @chmod($filename, 0660);
  }

  public function approveMail($hash) {
    throw new Exception("Not implemented yet");

    $files = self::_getFilesByHash($hash);

    if (!isset($files['mail']) ||
            !is_array($files['mail']) ||
            count($files['mail']) == 0) {
      return FALSE;
    }
    $approved = 0;
    foreach ($files['mail'] as $filename) {

      try {
        $_XMLSchema = @simplexml_load_file($filename);
      } catch (Exception $e) {
        $this->_Logger->log("Error opening/parsing file {$filename}", Logger::ERROR);
        return FALSE;
      }

      if ($_XMLSchema === FALSE) {
        $this->_Logger->log("Error opening/parsing file {$filename}", Logger::ERROR);
        return FALSE;
      }

      if (isset($_XMLSchema->approved)) {
        continue;
      } else {
        $_XMLSchema->addChild('approved', 'yes');
      }

      $file = @fopen($filename, 'w');
      if ($file === FALSE) {
        $this->_Logger->log("Chyba ukládání mailu do '{$filename}'", Logger::ERROR);
        return FALSE;
      }
      @fclose($file);

      $_doc = new DOMDocument('1.0', 'utf-8');
      $_doc->formatOutput = true;
      $_domnode = dom_import_simplexml($_XMLSchema);
      $_domnode = $_doc->importNode($_domnode, true);
      $_domnode = $_doc->appendChild($_domnode);

      $_doc->save($filename, LIBXML_NOEMPTYTAG);

      @chmod($filename, 0660);
      $approved++;
    }
    return $approved;
  }

  /**
   *
   * @param Zend_Mail $mail 
   */
  private function processStyles(Zend_Mail $mail) {
    throw new Exception("Not implemented yet");

    $htmlBody = base64_decode($mail->getBodyHtml(true));
    // file_put_contents( 'mail1.html', $htmlBody );
    libxml_use_internal_errors(true);
    $html = new DOMDocument();
    $html->loadHTML($htmlBody);
    // file_put_contents( 'mail2.html', $html->saveHTML() );
    // check content type meta tag
    $found = false;
    $list = $html->getElementsByTagName('meta');
    $len = $list->length;
    for ($i = 0; $i < $len; $i++) {
      if ($list->item(0)->hasAttribute('http-equiv') &&
              strtolower($list->item(0)->getAttribute('http-equiv')) == 'content-type') {
        $found = true;
      }
    }
    if (!$found) {
      // meta not found, add charset
      $html = new DomDocument();
      $html->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
              . $htmlBody);
    }

    $list = $html->getElementsByTagName('style');
    $style = '';
    if ($list->length > 0) {
      foreach ($list as $styleEl) {
        $style .= $styleEl->textContent . "\n";
        $styleEl->parentNode->removeChild($styleEl);
      }
    }

    $head = $html->createElement("head");
    $list = $html->getElementsByTagName('meta');
    foreach ($list as $item) {
      $head->appendChild($item);
    }

    $list = $html->getElementsByTagName('title');
    foreach ($list as $item) {
      $head->appendChild($item);
    }

    $list = $html->getElementsByTagName('body');
    if ($list->length > 0) {
      $list->item(0)->parentNode->insertBefore($head, $list->item(0));
    } else {
      $html->appendChild($head);
    }
    $html->normalize();

    $inline = new SCG_CSSToInlineStyles($html->saveHTML(), $style);
    // $inline->setCleanup( true );
    $newBody = $inline->convert();

    // file_put_contents( 'mail.html', $str );
    $mail->setBodyHtml($newBody, 'utf-8', Zend_Mime::ENCODING_BASE64);
  }

  /**
   * Vrati informace o vsech mail souborech v adresari data/mails
   * @return array [ 'wdate' => cekajici na publish, 'wappr'=> cekajici na approve, 'wtest' => cekaji na test
   *  'error' => chyby v mailech,
   * [ info] => array (
   * 'name' => jmeno souboru s mailem
   * 'status' => chyba/ceka
   * 'date' => datum odeslani / ihned pokud je prazdne
   * 'mails' => pocet mailu v danem "souboru"
   * 'subject' => subjekt mailu
   * 'recipients' => pole string prijemcu mailu
   * 'created' => datum vytvoreni souboru
   */
  public function getInfo() {

    $path = $this->_DataPath;

    $mailsWaitingDate = 0;
    $mailsWaitingApproval = 0;
    $mailsWaitingTest = 0;
    $errors = 0;
    $mailsInfo = array();
    $directory = dir($path);
    while (( $entry = $directory->read() ) !== FALSE) {

      if (substr($entry, -4) == '.xml') {
        $filename = $path . '/' . $entry;

        $fstat = '';
        $fp = @fopen($filename, 'r');
        if ($fp !== FALSE) {
          $fstat = @fstat($fp);
          @fclose($fp);
        }
        $created = '';
        if (isset($fstat['ctime'])) {
          $created = date('Y.m.d H:i:s', $fstat['ctime']);
        }

        $data = array(
            'name' => $entry,
            'status' => self::STATUS_ERROR,
            'date' => '',
            'error' => '',
            'mails' => '-',
            'subject' => '',
            'recipients' => array(),
            'created' => $created,
        );

        try {
          $_XMLSchema = @simplexml_load_file($filename);
        } catch (Exception $e) {
          $data['status'] = self::STATUS_ERROR;
          $data['error'] = 'Error loading xml';
          $mailsInfo[] = $data;
          $errors++;
          continue;
        }

        if ($_XMLSchema === FALSE) {
          $data['status'] = self::STATUS_ERROR;
          $data['error'] = 'Error processing xml';
          $mailsInfo[] = $data;
          $errors++;
          continue;
        }

        $cile = $_XMLSchema->xpath('recipients/*/*');

        $tmp = preg_split('/[-_]/', $entry);
        if (count($tmp) > 2) {
          $hash = $tmp[1];
          $entry = $tmp[0] . '_' . $tmp[1];
        } else {
          $hash = $entry;
        }

        if (isset($_XMLSchema->error)) {
          $data['error'] = (string) $_XMLSchema->error;
        }

        if (isset($_XMLSchema->approved) && ((string) $_XMLSchema->approved ) == 'yes') {
          if (isset($_XMLSchema->error)) {
            $data['status'] = self::STATUS_ERROR;
            $errors++;
          } else {
            $mailsWaitingDate++;
            $data['status'] = self::STATUS_WAIT_PUBLISH;
          }
        } else if (isset($_XMLSchema->test_sent)) {
          $mailsWaitingApproval++;
          $data['status'] = self::STATUS_WAIT_APPROVE;
        } else {
          $mailsWaitingTest++;
          $data['status'] = self::STATUS_WAIT_TEST;
        }

        if (isset($_XMLSchema->publish_timestamp)) {
          $date = date('Y-m-d H:i', (int) $_XMLSchema->publish_timestamp);
        } else {
          $date = 'ihned';
        }

        if (isset($mailsInfo[$hash])) {
          // nasli sme stejnej hash, spojit cile

          $mailsInfo[$hash]['recipients'] = array_merge($mailsInfo[$hash]['recipients'], array_map('strval', $cile));
          $mailsInfo[$hash]['mails'] = $mailsInfo[$hash]['mails'] + 1;
          // spocitat max pocet mailu podle cisla na konci jmena souboru
          if (count($tmp) > 3) {
            $mailsInfo[$hash]['total'] = max($mailsInfo[$hash]['total'], 1 + (int) $tmp[3]);
          }

          // test status prebijou ostatni
          if ($data['status'] == self::STATUS_WAIT_TEST) {
            $mailsInfo[$hash]['status'] = $data['status'];
          } else if ($data['status'] == self::STATUS_WAIT_APPROVE &&
                  $mailsInfo[$hash]['status'] != self::STATUS_WAIT_TEST) {
            // wapprove status prebije wpublish
            $mailsInfo[$hash]['status'] = $data['status'];
          }
        } else {
          $data['name'] = $entry;
          $data['date'] = $date;
          $data['mails'] = 1;
          if (count($tmp) > 3) {
            $data['total'] = 1 + (int) $tmp[3];
          }

          $data['subject'] = strip_tags((string) $_XMLSchema->subject);
          $data['recipients'] = array_map('strval', $cile);
          $mailsInfo[$hash] = $data;
        }
      }
    }

    $directory->close();
    return array(
        self::STATUS_WAIT_PUBLISH => $mailsWaitingDate,
        self::STATUS_WAIT_APPROVE => $mailsWaitingApproval,
        self::STATUS_WAIT_TEST => $mailsWaitingTest,
        'errors' => $errors,
        'info' => $mailsInfo,
    );
  }

  public function sendTestMail($hash) {
    throw new Exception("Not implemented yet");

    $files = self::_getFilesByHash($hash);
    if (!isset($files['mail']) ||
            !is_array($files['mail']) ||
            count($files['mail']) == 0) {
      $this->_Logger->log("Mail nenalezen", Logger::ERROR);
      return FALSE;
    }

    return $this->_sendMail(current($files['mail']), true);
  }

  /**
   * Vrati seznam souboru ktere maji shodny hash
   * @param string $hash Hledany hash
   * @return array Pole [ 'mail' => pole mail souboru, 'file' => pole file souboru, 'other' => pole jinych se stejnym hashem ]
   */
  protected function _getFilesByHash($hash) {
    $path = $this->_DataPath;

    $files = array(
        'mail' => array(),
        'file' => array(),
        'other' => array(),
    );

    $directory = dir($path);
    while (( $entry = $directory->read() ) !== FALSE) {
      $filename = $path . '/' . $entry;
      if (substr($entry, -4) == '.xml') {
        $entry = substr($entry, 0, -4);
      }
      $tmp = preg_split('/[-_]/', $entry);
      if (count($tmp) > 2) {
        $fileHash = $tmp[1];

        if ($hash == $fileHash) {
          switch ($tmp[2]) {
            case 'mail' :
            case 'file' :
              $files[$tmp[2]][] = $filename;
              break;
            default :
              if ( count( $tmp) > 3 ) {
                switch( $tmp[3]) {
                  case 'mail' :
                  case 'file' :
                    $files[$tmp[3]][] = $filename;
                    break;
                }
              } else {
                $files['other'][] = $filename;
              }
              break;
          }
        }
      }
    }

    $directory->close();
    return $files;
  }

  /**
   * Smaze soubory mailu se zadanym hashem
   * @param string $hash
   * @return array [ 'mail' => pocet smazanych mail souboru, 'file' => pocet smazanych priloh, 'other' => ostatni nezjistene soubory ]
   */
  public function removeMail($hash) {
    throw new Exception("Not implemented yet");

    if (trim($hash) == '') {
      $this->_Logger->log("Prázdný hash", Logger::ERROR);
      return false;
    }

    $files = self::_getFilesByHash($hash);

    $errors = 0;
    $deletedMails = 0;
    $deletedFiles = 0;
    $deletedOther = 0;
    foreach ($files as $type => $list) {
      foreach ($list as $filePath) {
        try {
          unlink($filePath);
          switch ($type) {
            case 'mail' :
              $deletedMails++;
              break;
            case 'file' :
              $deletedFiles++;
              break;
            default :
              $deletedOther++;
              break;
          }
        } catch (Exception $e) {
          $errors++;
        }
      }
    }

    return array(
        'mail' => $deletedMails,
        'file' => $deletedFiles,
        'other' => $deletedOther,
    );
  }

  /**
   * Nastavi log pro zapis informaci	
   * @param \Nette\Diagnostics\Logger $log log pro zapis informaci
   * */
  public function setLogger(\Nette\Diagnostics\Logger $log) {
    $this->_Logger = $log;
  }

  public function getMailHtml($hash) {
    if (trim($hash) == '') {
      $this->_Logger->log("Prázdný hash", Logger::ERROR);
      return FALSE;
    }

    $files = self::_getFilesByHash($hash);
    if (!isset($files['mail']) ||
            !is_array($files['mail']) ||
            count($files['mail']) == 0) {
      $this->_Logger->log("Mail nenalezen", Logger::ERROR);
      return FALSE;
    }

    $filename = current($files['mail']);

    try {
      $_XMLSchema = @simplexml_load_file($filename);
    } catch (Exception $e) {
      $this->_Logger->log("Error opening {$filename}", Logger::ERROR);
      return FALSE;
    }

    if ($_XMLSchema === FALSE) {
      $this->_Logger->log("Error parsing file {$filename}", Logger::ERROR);
      return FALSE;
    }

    $htmlBody = html_entity_decode((string) $_XMLSchema->body, ENT_QUOTES, 'UTF-8');

    // FIXME : odjebavka akcniho TinyMCE co nam kazi ctvrtecni vecery 
    $htmlBody = str_replace(array('<!--', '-->'), '', stripslashes($htmlBody));
    return $htmlBody;
  }

  public function generateZip($skolyIDs, $selectText, \Nette\Security\Identity $User, $turnaje) {
    
    $archive = new MailZipArchive();
    try {
      $file = $archive->createZipTemplate(
              $this->_MailPreparePath, 
              $User,
              $turnaje,
              $skolyIDs,
              $selectText );
    } catch (\SCG\MailZipArchiveException $e) {
      $this->_Logger->log( "Error generating zip template {$zipFileName} : " . $e->getMessage() );
      \Nette\Diagnostics\Debugger::barDump( $e->getMessage(), "Error" );
      return false;
    }
    
    return $file;
  }

  public function getPreparedZips() {
    
    $archive = new MailZipArchive();
    try{
      $archives = $archive->getPreparedTemplates( $this->_MailPreparePath, $this->_Logger );
      
    } catch ( \SCG\MailZipArchiveException $e ) {
      $this->_Logger->log( "Error getting zip templates : " . $e->getMessage() );
      \Nette\Diagnostics\Debugger::barDump( $e->getMessage(), "Error" );
      return false;
    }
    
    return $archives;
  }
  
  /**
   * Nacte Zip archiv podle jmena
   * @param string $zipFileName
   * @return boolean|\SCG\MailZipArchive
   */
  protected function getZipFile( $zipFileName ) {
    $archive = new MailZipArchive();
    try {
      $archive->openZipFile( $this->_MailPreparePath, $zipFileName );
    } catch( MailZipArchiveException $e ) {
      $this->_Logger->log( "Error : " . $e->getMessage() );
      \Nette\Diagnostics\Debugger::barDump( $e->getMessage(), "Error" );
      return false;
    }
    return $archive;
  }
  
  /**
   * Vrati plnou cestu k zip souboru, pro poslani uzivateli
   * @param string $zipFile
   * @return string
   */
  public function getFullZipPath( $zipFile ) {
    return $this->_MailPreparePath . $zipFile;
  }
  
  public function prepareMails( $AkceID, $formData, \Nette\Security\Identity $User, TurnajRepository $turnajRepo, SkolaRepository $skolaRepo, OsobaRepository $osobaRepo ) {
    
    $zipFileArchive = $this->getZipFile( $formData['zip'] );
    $skolyIDs = $zipFileArchive->getSkoly();
    
    $originalBody = $formData['body'];
    
    // nacte informace o jednotlivych turnajich pro vybrane skoly
    $turnaje = $turnajRepo->fetchForMailsWithJoins( $AkceID, $skolyIDs);
    
    $skolySpadovky = $turnajRepo->fetchSpadovkyForSkoly( $AkceID, $skolyIDs);
    // \Nette\Diagnostics\Debugger::barDump( $skolySpadovky, "Spadovky skol [turnaj_id] = array( skola_id)" );
    
    $formData['hash'] = md5( time() );
    
    foreach( $turnaje as $turnaj ) {
      
      if ( !isset( $skolySpadovky[ $turnaj->turnaj_id]) ) {
        $this->_Logger->log( "Chybějící školy pro turnaj {$turnaj->turnaj_id}". Logger::WARNING );
        continue;
      }
      $formData['prefix'] = $turnaj->turnaj_id;
      
      $formData['skoly'] = $skolySpadovky[ $turnaj->turnaj_id ];
      $cas = \Nette\DateTime::from( $turnaj->datum_cas );
      
      $formData['from_mail'] = str_replace( '@scg.cz', '', $turnaj->kk_email);
      $formData['from_label'] = $turnaj->kk_krestni . ' ' . $turnaj->kk_prijmeni;
      
      // vytvor html sablonu ( nahradit koordinatora, skolu, turnaj )
      $formData['body'] = str_replace(
              array( '&lt;KRAJSKY_KOORDINATOR&gt;', '&lt;DATUM_A_CAS&gt;', '&lt;PORADATELSKA_SKOLA&gt;'),
              array( "<p>{$turnaj->kk_krestni} {$turnaj->kk_prijmeni}</p>
<p>Krajský koordinátor pro kraj: {$turnaj->kraj}</p>
<p>Telefon: {$turnaj->kk_mobil}</p>
<p>E-mail: {$turnaj->kk_email}</p>",
        strftime( '%H:%M %d.%m.%Y', $cas->format('U')),
        "<p>{$turnaj->skola_nazev} - {$turnaj->skola_ulice}, {$turnaj->skola_mesto}, PSČ {$turnaj->skola_psc}</p>"),
        $originalBody
      );
      
      // vynuluj soubory
      unset( $formData['attachements'] );
      $files = $zipFileArchive->getPrilohyProTurnaj( $turnaj->turnaj_id );
      // rozbal prilohy
      if ( count( $files ) > 0 ) {
        $extracted = $zipFileArchive->extractToTemp( $this->_MailPreparePath . 'tmp/', $files );
        $formData['attachements'] = $extracted;
      } else {
        $this->_Logger->log( "Turnaj ID {$turnaj->turnaj_id} : Kraj {$turnaj->kraj}, město {$turnaj->mesto}, nemá přílohu v zipu {$formData['zip']}", Logger::ERROR );
      }
      
      // vytvor xml mailu
      $this->addSkolyJob( $formData, $User, $skolaRepo, $osobaRepo);
      // \Nette\Diagnostics\Debugger::barDump( "Vytvořen mail pro turnaj ID {$turnaj->turnaj_id} : Kraj {$turnaj->kraj}, město {$turnaj->mesto}");
      $this->_Logger->log( "Vytvořen mail pro turnaj ID {$turnaj->turnaj_id} : Kraj {$turnaj->kraj}, město {$turnaj->mesto}", 'success' );
      
      // odstran temp soubory
      if ( count( $files ) > 0 ) {
        $zipFileArchive->deleteTemp( $extracted );
      }
      
    }
    return true;
  }
  
}
