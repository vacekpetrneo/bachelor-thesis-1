<?php

namespace SCG;

use Nette\Database\Reflection\ConventionalReflection;

/**
 * Description of DBReflection
 *
 * @author rattus
 */
class DBReflection extends ConventionalReflection {
	
	public function __construct() {
		parent::__construct( '%s_id', '%s_id', '%s');
	}
	
}
