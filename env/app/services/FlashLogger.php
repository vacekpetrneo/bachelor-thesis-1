<?php

namespace SCG;

/**
 * Description of FlashLogger
 *
 * @author rattus
 */
class FlashLogger extends \Nette\Diagnostics\Logger {
  
  /** @var Nette\Application\UI\Presenter */
  private $presenter;
  
  /**
   * Create new FlashLogger instance, use presenter for flash messages
   * @param Nette\Application\UI\Presenter $presenter
   */
  public function __construct(\Nette\Application\UI\Presenter $presenter) {
    $this->presenter = $presenter;
  }
  
  /**
	 * Shows message in flash message
	 * @param  string|array
	 * @param  int     one of constant INFO, WARNING, ERROR, CRITICAL
	 * @return bool    was successful?
	 */
  public function log($message, $priority = self::INFO) {
    $this->presenter->flashMessage( $message, $priority);
    return true;
  }
  
}
