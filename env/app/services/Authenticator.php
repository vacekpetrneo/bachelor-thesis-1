<?php

namespace SCG;

use Nette, Nette\Security\AuthenticationException;

/**
 * Users authenticator.
 */
class Authenticator extends Nette\Object implements Nette\Security\IAuthenticator {

	/** @var UserRepository */
	private $userRepo;

	public function __construct(UserRepository $database) {
		$this->userRepo = $database;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials) {
		list($username, $password) = $credentials;
		$row = $this->userRepo->findByLogin($username);

		if (!$row) {
			throw new AuthenticationException('Chybné jméno nebo heslo.', self::IDENTITY_NOT_FOUND);
		}

		if ($row->pass !== $this->userRepo->calculateHash($password, $row->salt)) {
			throw new AuthenticationException('Chybné jméno nebo heslo.', self::INVALID_CREDENTIAL);
		}

		$arr = $row->toArray();
		unset($arr['pass']);
		unset($arr['salt']);

		$roles = $this->userRepo->getUserRoles($row->uzivatel_id);

		return new Nette\Security\Identity($row->uzivatel_id, $roles, $arr);
	}
  
  /**
   * Zmeni heslo uzivatele
   * @param int $id
   * @param string $Password
   * @return int | FALSE
   */
  public function changePassword( $id, $Password) {
    return $this->userRepo->setNewPassword($id, $Password);
  }

}
