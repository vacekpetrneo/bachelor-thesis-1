<?php

namespace SCG;

class KrajRepository extends Repository {

  /**
   * @return Nette\Database\Table\Selection
   */     
  public function findAll() {
    return $this->getTable();
  }

	/**
	 * @return Nette\Database\Row
	 */
	public function findById($krajId) {
		return $this->findAll()->where('kraj.kraj_id = ?', $krajId)->fetch();
	}
	
	public function fetchPossible() {
		return $this->getTable()->order('nazev ASC')->fetchPairs( 'kraj_id', 'nazev');
	}

}
