<?php

namespace SCG;

class SkolaRepository extends Repository {

  const TYPY_TABLE = 'skola_typ';
  const TYPY_VAZBA_TABLE = 'skola_typ_vazba';
  const LOG_TABLE = 'skola_log';

  /**
   * Vrátí všechny školy i s joiny
   * @return Nette\Database\Table\Selection
   */
  public function findAll() {
    return $this->getTable()
                ->select('skola.*')
                ->select('okres.kraj_id AS kraj_id')
                ->select('okres.nazev AS okres_nazev')
                ->where('skola.okres_id = okres.okres_id');  
  }

  /**
   * Najde skolu podle ID, pripadne vice skol pokud dostane pole
   * @return Nette\Database\Row | Nette\Database\Table\Selection
   */
  public function findById($skolaID) {
    $result = $this->findAll()->where('skola.skola_id', $skolaID);
    if (is_array($skolaID)) {
      return $result;
    }
    return $result->fetch();
  }

  /**
   * Nacte pole typu skol k uvedenym skolam
   * @param array $SkolyIDs
   * @return array
   */
  public function fetchTypy(array $SkolyIDs) {
    return $this->connection->table(self::TYPY_VAZBA_TABLE)->select("skola_typ_id, skola_id")
                    ->where('skola_id', $SkolyIDs)
                    ->fetchPairs('skola_id', 'skola_typ_id');
  }

  /**
   * Vrati skoly ktere patri do nejake spadove oblasti v dane akci
   * @param int $AkceID
   * @return Nette\Database\Selection
   */
  public function fetchSpadovky($AkceID) {
    $select = $this->getTable()->select('skola.*')
            ->where(':spadovka.turnaj.uroven.akce_id = ?', (int) $AkceID);
    return $select;
  }

  /**
   * Provede fetchSearch ale omezi vysledek pouze na skoly ktere jsou soucasti spadove oblasti turnaje.
   * @param array $Options
   * @param string $Order
   * @param array $Ucasti
   * @param bool $OtevreneTurnajeOnly Default - vrati pouze spadovky k otevrenym turnajum, false - vrati vsechny spadovky nehlede na stav turnaju
   * @return \Nette\Database\Table\Selection
   */
  public function fetchSearchSpadovkyOnly( $AkceID, $Options, $Order, $Ucasti = null, $OtevreneTurnajeOnly = true) {
    $select = $this->fetchSearch($Options, $Order, $Ucasti);
    // Pokud hledame poradatelske, vratit vsechny bez ohledu na spadovky
    if (isset($Options['poradatelske_akce_id']) && is_array( $Options['poradatelske_akce_id']) && count( $Options['poradatelske_akce_id']) > 0 ) {
      return $select;
    }
    $spadovkySel = $this->connection->table('spadovka')->select('DISTINCT spadovka.skola_id')->where('turnaj.uroven.akce_id = ?', $AkceID);
    if ($OtevreneTurnajeOnly) {
      $spadovkySel->where('turnaj.stav = ?', TurnajRepository::STATUS_OPEN);
    }
    return $select->where('skola.skola_id IN', array_values( $spadovkySel->fetchPairs('skola_id', 'skola_id') ));
  }

  /**
   * Vrati popis vybranych voleb v options
   * @param array $Options
   * @param array $akce Pole akci a nazvu
   * @return string
   */
  public function optionsToText($Options, $akce) {
    $ret = array();
    if (isset($Options['skola_typ_id']) &&
            is_array($Options['skola_typ_id']) &&
            count($Options['skola_typ_id']) > 0) {
      $vybraneTypy = $this->replaceIDsToNames($Options['skola_typ_id'], $this->fetchPossibleTypes());
      $ret[] = "typy škol '" . implode("','", $vybraneTypy) . "'";
    }

    if (isset($Options['poradatelske_akce_id']) && is_array($Options['poradatelske_akce_id']) && count($Options['poradatelske_akce_id']) > 0) {
      $vybraneAkce = $this->replaceIDsToNames($Options['poradatelske_akce_id'], $akce);
      $ret[] = "pořadatelské pro akce '" . implode("','", $vybraneAkce) . "'";
    }

    if (isset($Options['neporadatelske_akce_id']) && is_array($Options['neporadatelske_akce_id']) && count($Options['neporadatelske_akce_id']) > 0) {
      $vybraneAkce = $this->replaceIDsToNames($Options['neporadatelske_akce_id'], $akce);
      $ret[] = "nepořadatelské pro akce '" . implode("','", $vybraneAkce) . "'";
    }

    if (isset($Options['ucast_akce_id']) && is_array($Options['ucast_akce_id']) && count($Options['ucast_akce_id']) > 0) {
      $vybraneAkce = $this->replaceIDsToNames($Options['ucast_akce_id'], $akce);
      $ret[] = "učast na akci  '" . implode("','", $vybraneAkce) . "'";
    }

    if (isset($Options['neucast_akce_id']) && is_array($Options['neucast_akce_id']) && count($Options['neucast_akce_id']) > 0) {
      $vybraneAkce = $this->replaceIDsToNames($Options['neucast_akce_id'], $akce);
      $ret[] = "neučast na akci  '" . implode("','", $vybraneAkce) . "'";
    }

    if (isset($Options['tymu_1']) && is_array($Options['tymu_1']) && count($Options['tymu_1']) > 0) {
      $vybraneAkce = $this->replaceIDsToNames($Options['tymu_1'], $akce);
      $ret[] = "1 tým v akci '" . implode("','", $vybraneAkce) . "'";
    }

    if (isset($Options['tymu_2']) && is_array($Options['tymu_2']) && count($Options['tymu_2']) > 0) {
      $vybraneAkce = $this->replaceIDsToNames($Options['tymu_2'], $akce);
      $ret[] = "2 tým v akci '" . implode("','", $vybraneAkce) . "'";
    }

    if (isset($Options['tymu_3']) && is_array($Options['tymu_3']) && count($Options['tymu_3']) > 0) {
      $vybraneAkce = $this->replaceIDsToNames($Options['tymu_3'], $akce);
      $ret[] = "3 tým v akci '" . implode("','", $vybraneAkce) . "'";
    }

    if (isset($Options['tymu_4']) && is_array($Options['tymu_4']) && count($Options['tymu_4']) > 0) {
      $vybraneAkce = $this->replaceIDsToNames($Options['tymu_4'], $akce);
      $ret[] = "4 tým v akci '" . implode("','", $vybraneAkce) . "'";
    }

    return implode(';', $ret);
  }

  /**
   * Prevede pole ID na hodnoty podle prevodniho pole
   * @param array $aIDs Pole int hodnot ID
   * @param array $idValues Pole id => hodnota
   * @return array
   */
  protected function replaceIDsToNames(array $aIDs, array $idValues) {
    $ret = array();
    foreach ($aIDs as $id) {
      $ret[] = $idValues[$id];
    }
    return $ret;
  }

  /**
   * Hleda skoly podle dat z formulare
   * @param array $Options
   * @param string|array $Order
   * @param array|null $Ucasti
   * @return Nette\Database\Selection
   */
  public function fetchSearch($Options, $Order, $Ucasti = null) {
    // NDebugger::barDump( $Options, "Search options" );
    if (!is_array($Options)) {
      $Options = array($Options);
    }

    $select = $this->getTable()->select('DISTINCT skola.*');

    if (isset($Options['kraj_id']) &&
            ( (int) $Options['kraj_id'] != 0 ||
            ( is_array($Options['kraj_id']) && count($Options['kraj_id']) > 0) )) {

      if (is_array($Options['kraj_id'])) {
        $ids = array_map('intval', $Options['kraj_id']);
        $select->where('okres.kraj.kraj_id', $ids);
      } else {
        $select->where('okres.kraj.kraj_id', (int) $Options['kraj_id']);
      }
    }

    #where for skola_typ
    if (isset($Options['skola_typ_id']) &&
            is_array($Options['skola_typ_id']) &&
            count($Options['skola_typ_id']) > 0) {

      $ids = array_map('intval', $Options['skola_typ_id']);
      $select->where( ':' . self::TYPY_VAZBA_TABLE . '.skola_typ_id', $ids);
    }

    # where for region
    if (isset($Options['region_id']) &&
            is_array($Options['region_id']) &&
            count($Options['region_id']) > 0) {
      $ids = array_map('intval', $Options['region_id']);
      $select->where('okres.kraj:kraj_vazba.region_id', $ids);
    }

    # where for okres
    if (isset($Options['okres_id'])) {
      if (is_array($Options['okres_id']) && count($Options['okres_id']) > 0) {
        $ids = array_map('intval', $Options['okres_id']);
        $select->where('okres.okres_id', $ids);
      } elseif ((int) $Options['okres_id'] != 0) {
        $select->where('okres.okres_id', (int) $Options['okres_id']);
      }
    }

    #where for mesto
    if (isset($Options['mesto']) && trim($Options['mesto']) != '') {
      // $mesto = $select->getConnection()->getSupplementalDriver()->formatLike( strtolower( $Options['mesto']), 0 );
      // $sqlLiteral = new NSqlLiteral( 'skola.mesto LIKE CONVERT(_UTF8 '. $mesto . ' USING cp1250) COLLATE cp1250_general_ci' );
      // $sqlLiteral = new NSqlLiteral( "skola.mesto LIKE {$mesto}" );
      //NDebugger::barDump( $sqlLiteral );
      // $select->where( $sqlLiteral );
      // NDebugger::barDump( $select );
      $select->where('LOWER( skola.mesto) LIKE ?', strtolower('%' . $Options['mesto'] . '%'));
    }

    #where for nazev
    if (isset($Options['nazev']) && trim($Options['nazev']) != '') {
      $select->where('LOWER( skola.nazev) LIKE ?', strtolower('%' . $Options['nazev'] . '%'));
    }

    #pridat pokud jsou vybrane spadove oblasti ( turnaje )
    if (isset($Options['spadovky']) && is_array($Options['spadovky']) && count($Options['spadovky']) > 0) {
      $ids = array_map('intval', $Options['spadovky']);
      $select->where(':spadovka.turnaj.turnaj_id IN', $ids);
    }

    #pridat pokud je vybrane skoly bez spadovek pro akci
    if (isset($Options['nemaji_spadovku_akce_id']) && is_array($Options['nemaji_spadovku_akce_id']) && count($Options['nemaji_spadovku_akce_id']) > 0) {
      $ids = array_map('intval', $Options['nemaji_spadovku_akce_id']);
      $skolySeSpadovkou = $this->connection->table('akce')
                      ->where('akce.akce_id IN', $ids)
                      ->where(':uroven:turnaj:spadovka.skola_id IS NOT NULL')
                      ->select('spadovka.skola_id')->fetchPairs('skola_id', 'skola_id');
//    	$select->where( 'skola.skola_id NOT IN ', $skolySeSpadovkou );
      if (isset($Options['skoly_exclude'])) {
        // prunik s existujicim filtrem ( AND )
        $Options['skoly_exclude'] = array_values(array_intersect($Options['skoly_exclude'], array_values($skolySeSpadovkou)));
      } else {
        $Options['skoly_exclude'] = array_values($skolySeSpadovkou);
      }
    }

    #pridat pokud je vybrane skoly ktere maji spadovku pro akci
    if (isset($Options['maji_spadovku_akce_id']) && is_array($Options['maji_spadovku_akce_id']) && count($Options['maji_spadovku_akce_id']) > 0) {
      $ids = array_map('intval', $Options['maji_spadovku_akce_id']);
      $skolySeSpadovkou = $this->connection->table('akce')
                      ->where('akce.akce_id IN', $ids)
                      ->where(':uroven:turnaj:spadovka.skola_id IS NOT NULL')
                      ->select('spadovka.skola_id')->fetchPairs('skola_id', 'skola_id');
//      $select->where( 'skola.skola_id IN ', $skolySeSpadovkou );

      if (isset($Options['skoly_filter'])) {
        // prunik s existujicim filtrem ( AND )
        $Options['skoly_filter'] = array_values(array_intersect($Options['skoly_filter'], array_values($skolySeSpadovkou)));
      } else {
        $Options['skoly_filter'] = array_values($skolySeSpadovkou);
      }
    }

    #odebrat ty co uz jsou ve spadovce pro danou uroven ( akci ) - pro editaci spadovek, schova jiz pridane
    if (isset($Options['spadovky_uroven']) && (int) $Options['spadovky_uroven'] != 0) {
      // FIXME - check
      $ids = array_map('intval', $Options['spadovky_uroven']);
      $select->where(':spadovka.turnaj.uroven_id IN', $ids);
      /*
        $spadovkaTable = $this->connection->table('spadovka')->select( 'skola_id')->where( 'turnaj.uroven_id', (int) $Options['spadovky_uroven'] );
        $spadoveSkoly = $spadovkaTable->fetchPairs( 'skola_id', null);

        if ( count( $spadoveSkoly) > 0 ) {
        $ids = array_map( 'intval', $spadoveSkoly );
        $select->where( 'skola.skola_id NOT ', $ids );
        }
       */
    }

    // poradatelske pro akce
    if (isset($Options['poradatelske_akce_id']) && is_array($Options['poradatelske_akce_id']) && count($Options['poradatelske_akce_id']) > 0) {
      // pouze poradatelske skoly
      $ids = array_map('intval', $Options['poradatelske_akce_id']);
      $select->where(':turnaj.uroven.akce_id IN', $ids);
    }

    // neporadatelske pro akce
    if (isset($Options['neporadatelske_akce_id']) && is_array($Options['neporadatelske_akce_id']) && count($Options['neporadatelske_akce_id']) > 0) {
      // pouze neporadatelske skoly
      $ids = array_map('intval', $Options['neporadatelske_akce_id']);
      $skolyPoradatelskeProAkce = $this->connection->table('akce')
              ->where('akce.akce_id IN', $ids)
              ->select(':uroven:turnaj.skola_id')
              ->fetchPairs('skola_id', 'skola_id');

      if (isset($Options['skoly_exclude'])) {
        // prunik s existujicim filtrem ( AND )
        $Options['skoly_exclude'] = array_values(array_intersect($Options['skoly_exclude'], array_values($skolyPoradatelskeProAkce)));
      } else {
        $Options['skoly_exclude'] = array_values($skolyPoradatelskeProAkce);
      }
    }

    // zpracujeme si ucasti skol pro rychlejsi porovnani s akcemi
    if ($Ucasti != null && is_array($Ucasti)) {
      $akceUcastSkola = array();
      foreach ($Ucasti as $skola_id => $skolaUcast) {
        foreach ($skolaUcast as $ucastData) {
          if (!isset($akceUcastSkola[$ucastData['id']])) {
            $akceUcastSkola[$ucastData['id']] = array();
          }
          $akceUcastSkola[$ucastData['id']][] = $skola_id;
        }
      }

      // [akce_id] => [ pocet_tymu ] => [] skola_id
      $akceUcastSkolaTymy = array();
      foreach ($Ucasti as $skola_id => $skolaUcast) {
        foreach ($skolaUcast as $ucastData) {
          if (!isset($akceUcastSkolaTymy[$ucastData['id']])) {
            $akceUcastSkolaTymy[$ucastData['id']] = array();
          }
          if (!isset($akceUcastSkolaTymy[$ucastData['id']][(int) $ucastData['pocet']])) {
            $akceUcastSkolaTymy[$ucastData['id']][(int) $ucastData['pocet']] = array();
          }
          $akceUcastSkolaTymy[$ucastData['id']][(int) $ucastData['pocet']][] = $skola_id;
        }
      }
      // Nette\Diagnostics\Debugger::barDump( $akceUcastSkolaTymy );
    }

    if (isset($Options['tymu_1']) && is_array($Options['tymu_1']) && count($Options['tymu_1']) > 0) {
      $ucastnici = array();
      foreach ($Options['tymu_1'] as $akce_id) {
        if (isset($akceUcastSkolaTymy[(int) $akce_id]) && isset($akceUcastSkolaTymy[(int) $akce_id][1])) {
          $ucastnici = array_merge($ucastnici, $akceUcastSkolaTymy[(int) $akce_id][1]);
        }
      }
      if (isset($Options['skoly_filter'])) {
        // prunik s existujicim filtrem ( AND )
        $Options['skoly_filter'] = array_values(array_intersect($Options['skoly_filter'], $ucastnici));
      } else {
        $Options['skoly_filter'] = $ucastnici;
      }
    }

    if (isset($Options['tymu_2']) && is_array($Options['tymu_2']) && count($Options['tymu_2']) > 0) {
      $ucastnici = array();
      foreach ($Options['tymu_2'] as $akce_id) {
        if (isset($akceUcastSkolaTymy[(int) $akce_id]) && isset($akceUcastSkolaTymy[(int) $akce_id][2])) {
          $ucastnici = array_merge($ucastnici, $akceUcastSkolaTymy[(int) $akce_id][2]);
        }
      }
      if (isset($Options['skoly_filter'])) {
        // prunik s existujicim filtrem ( AND )
        $Options['skoly_filter'] = array_values(array_intersect($Options['skoly_filter'], $ucastnici));
      } else {
        $Options['skoly_filter'] = $ucastnici;
      }
    }

    if (isset($Options['tymu_3']) && is_array($Options['tymu_2']) && count($Options['tymu_2']) > 0) {
      $ucastnici = array();
      foreach ($Options['tymu_3'] as $akce_id) {
        if (isset($akceUcastSkolaTymy[(int) $akce_id]) && isset($akceUcastSkolaTymy[(int) $akce_id][3])) {
          $ucastnici = array_merge($ucastnici, $akceUcastSkolaTymy[(int) $akce_id][3]);
        }
      }
      if (isset($Options['skoly_filter'])) {
        // prunik s existujicim filtrem ( AND )
        $Options['skoly_filter'] = array_values(array_intersect($Options['skoly_filter'], $ucastnici));
      } else {
        $Options['skoly_filter'] = $ucastnici;
      }
    }

    if (isset($Options['tymu_4']) && is_array($Options['tymu_4']) && count($Options['tymu_4']) > 0) {
      $ucastnici = array();
      foreach ($Options['tymu_4'] as $akce_id) {
        if (isset($akceUcastSkolaTymy[(int) $akce_id]) && isset($akceUcastSkolaTymy[(int) $akce_id][4])) {
          $ucastnici = array_merge($ucastnici, $akceUcastSkolaTymy[(int) $akce_id][4]);
        }
      }
      if (isset($Options['skoly_filter'])) {
        // prunik s existujicim filtrem ( AND )
        $Options['skoly_filter'] = array_values(array_intersect($Options['skoly_filter'], $ucastnici));
      } else {
        $Options['skoly_filter'] = $ucastnici;
      }
    }


    #ucast/neucast v akcich
    if (isset($Options['ucast_akce_id']) && is_array($Options['ucast_akce_id']) && count($Options['ucast_akce_id']) > 0) {
      $ucastnici = array();
      foreach ($Options['ucast_akce_id'] as $akce_id) {
        if (isset($akceUcastSkola[(int) $akce_id])) {
          $ucastnici = array_merge($ucastnici, $akceUcastSkola[(int) $akce_id]);
        }
      }
      if (isset($Options['skoly_filter'])) {
        // prunik s existujicim filtrem ( AND )
        $Options['skoly_filter'] = array_values(array_intersect($Options['skoly_filter'], $ucastnici));
      } else {
        $Options['skoly_filter'] = $ucastnici;
      }
    }

    if (isset($Options['neucast_akce_id']) && is_array($Options['neucast_akce_id']) && count($Options['neucast_akce_id']) > 0) {
      $ucastnici = array();
      foreach ($Options['neucast_akce_id'] as $akce_id) {
        if (isset($akceUcastSkola[(int) $akce_id])) {
          $ucastnici = array_merge($ucastnici, $akceUcastSkola[(int) $akce_id]);
        }
      }
      if (isset($Options['skoly_exclude'])) {
        // prunik s existujicim filtrem ( AND )
        $Options['skoly_exclude'] = array_values(array_intersect($Options['skoly_exclude'], $ucastnici));
      } else {
        $Options['skoly_exclude'] = $ucastnici;
      }
    }

    #filtr pouze pouzite skoly
    if (isset($Options['skoly_filter']) &&
            is_array($Options['skoly_filter']) &&
            count($Options['skoly_filter']) > 0) {
      $ids = array_unique(array_map('intval', $Options['skoly_filter']));
      $select->where('skola.skola_id', $ids);
    }

    if (isset($Options['skoly_exclude']) &&
            is_array($Options['skoly_exclude']) &&
            count($Options['skoly_exclude']) > 0) {
      $ids = array_unique(array_map('intval', $Options['skoly_exclude']));
      $select->where('skola.skola_id NOT IN', $ids);
    }

    if ($Order != null) {
      $select->order($Order);
    }

    return $select;
  }

  /**
   * Vrati ucasti skol na akcich podle tymu
   * Vracene pole ma tvar [skola_id] = [ [id=>akce_id] [ nazev=>akce_nazev ]]
   * @return array Pole ucasti skol na akcich
   */
  public function fetchUcast() {

    $select = $this->connection->table('tym')->select('skola_id, tym.akce_id, akce.nazev AS nazev, COUNT( tym.tym_id) AS pocet')
            ->group('tym.akce_id, tym.skola_id');

    $result = array();
    foreach ($select as $item) {
      if (!isset($result[(int) $item->skola_id])) {
        $result[(int) $item->skola_id] = array();
      }
      $result[(int) $item->skola_id][] = array(
          'id' => $item->akce_id,
          'nazev' => $item->nazev,
          'pocet' => $item->pocet,
      );
    }
    return $result;
  }

  /**
   * Vrati pole typu skol, serazene podle nazvu
   * @return array
   */
  public function fetchPossibleTypes() {
    return $this->connection->table(self::TYPY_TABLE)->order('nazev ASC')->fetchPairs('skola_typ_id', 'nazev');
  }

  public function sloucit(array $skolyData, User $user) {
    if (!isset($skolyData['skoly_ids'])) {
      throw new Exception("Chybí id škol");
    }

    if (!isset($skolyData['primarni_skola'])) {
      throw new Exception("Chybí id primární školy");
    }

    $skolyIDs = explode(',', $skolyData['skoly_ids']);
    $primarniSkolaID = (int) $skolyData['primarni_skola'];

    $skolyData['skola_id'] = $primarniSkolaID;
    $this->update($skolyData, $user);

    $this->connection->beginTransaction();
    try {
      $logData = array();
      foreach ($skolyIDs as $id) {
        $logData[] = array(
            'skola_id' => $id,
            'uzivatel_id' => $user->getId(),
            'cas' => new \DateTime(),
            'zmena' => 'Sloučeny školy ' . $skolyData['skoly_ids'] . ' do ' . $primarniSkolaID
        );
      }

      $this->connection->table(self::LOG_TABLE)->insert($logData);

      // nemazat
      //$this->connection->table( 'kontaktni_osoba')->where( array( 'skola_id ' => $skolyIDs ) )->update( array( 'skola_id' => $primarniSkolaID ));
      //$this->connection->table( 'koordinator')->where( array( 'skola_id ' => $skolyIDs ) )->update( array( 'skola_id' => $primarniSkolaID ));
      //$this->connection->table( 'spadovka')->where( array( 'skola_id ' => $skolyIDs ) )->update( array( 'skola_id' => $primarniSkolaID ));
      //$this->connection->table( 'turnaj')->where( array( 'skola_id ' => $skolyIDs ) )->update( array( 'skola_id' => $primarniSkolaID ));
      //$this->connection->table( 'tym')->where( array( 'skola_id ' => $skolyIDs ) )->update( array( 'skola_id' => $primarniSkolaID ));
      //$this->connection->table( self::TYPY_VAZBA_TABLE )->where( array( 'skola_id ' => $skolyIDs ) )->where( 'skola_id != ?', $primarniSkolaID)->delete();
      $this->getTable()->where(array('skola_id ' => $skolyIDs))->where('skola_id != ?', $primarniSkolaID)->update(array('zruseno' => '1'));

      $this->connection->commit();
    } catch (Exception $e) {
      $this->connection->rollBack();
      throw $e;
    }
  }

  /**
   * Ulozi zmeny zaznamu skoly
   * @param array $skolaData
   */
  public function update(array $skolaData, User $user) {

    if (!isset($skolaData['skola_id'])) {
      throw new Exception("Chybí id školy");
    }
    $skola_id = (int) $skolaData['skola_id'];

    $skola = $this->findById($skola_id);
    $typy = $this->fetchTypy(array($skola_id));

    $skolaArray = $skola->toArray();
    $skolaArray['skola_typ_id'] = $typy[$skola_id];

    $change = array_intersect_key($skolaData, $skolaArray);
    foreach ($change as $key => $val) {
      if ($skolaArray[$key] == $val) {
        unset($change[$key]);
      }
    }

//		\Nette\Diagnostics\Debugger::barDump( $skolaData, "nova" );
//		\Nette\Diagnostics\Debugger::barDump( $skolaArray, "puvodni" );
//		\Nette\Diagnostics\Debugger::barDump( $change, "zmena" );

    $this->connection->beginTransaction();

    try {
      // pridat skola_id pro log
      $change['skola_id'] = $skola_id;
      $this->logChange($change, $skolaArray, $user);
      // skola_id se nemeni
      unset($change['skola_id']);
      // typy se meni separatne
      unset($change['skola_typ_id']);
      // nastavit upraveno na ted
      $change['upraveno'] = new \DateTime();

      $skola->update($change);

      $typy = array();
      foreach ($skolaData['skola_typ_id'] as $typ_id) {
        $typy[] = array('skola_id' => $skola_id, 'skola_typ_id' => $typ_id);
      }

      $this->connection->table(self::TYPY_VAZBA_TABLE)->where('skola_id', $skola_id)->delete();
      $this->connection->table(self::TYPY_VAZBA_TABLE)->insert($typy);

      $this->connection->commit();
    } catch (Exception $e) {
      $this->connection->rollBack();
      throw $e;
    }
  }

  /**
   * Ulozi zmeny zaznamu skoly do DB
   * @param array $data Nova data
   * @param array $oldData Stara data k porovnani
   * @param \SCG\User $user Uzivatel ktery provadi zmenu
   */
  protected function logChange($data, $oldData, User $user) {

    $table = $this->connection->table(self::LOG_TABLE);

    $log = array(
        'skola_id' => $data['skola_id'],
        'uzivatel_id' => $user->getId(),
        'cas' => new \DateTime(),
        'zmena' => '',
    );

    if (isset($data['uzivatel_id'])) {
      unset($data['uzivatel_id']);
    }
    if (isset($data['skola_id'])) {
      unset($data['skola_id']);
    }

    $cols = array_keys($data);
    unset($cols['skola_id']);

    foreach ($cols as $col) {
      $colName = ucfirst($col);
      if (isset($data[$col]) &&
              ( (!isset($oldData[$col]) && !empty($data[$col]) ) || $oldData[$col] != $data[$col] )) {
        $log['zmena'] .= "{$colName} : ";
        if (isset($oldData[$col])) {
          $log['zmena'] .= $oldData[$col];
        }
        if (is_array($data[$col])) {
          $log['zmena'] .= ' => ' . implode(',', $data[$col]) . ';';
        } else {
          $log['zmena'] .= ' => ' . $data[$col] . ';';
        }
      }
    }

    if ($log['zmena'] != '') {
      $table->insert($log);
    }
  }

}
