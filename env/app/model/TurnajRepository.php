<?php

namespace SCG;

use Nette;

class TurnajRepository extends Repository {

  const SOUPISKA_TABLE = 'soupiska';
  const VYSLEDKY_TABLE = 'vysledky';
  const SPADOVKA_TABLE = 'spadovka';
  const CHECKLIST_TABLE = 'checklist';
  const CHECKLIST_STAV_TABLE = 'checklist_stav';

  const STATUS_NEW = 0;
  const STATUS_OPEN = 1;
  const STATUS_CLOSED = 2;
  const STATUS_FINISHED = 3;
  const STATUS_CANCELED = 4;

  public function fetchPossibleStav() {
    return array( self::STATUS_NEW => 'Vytvořený',
                  self::STATUS_OPEN => 'Otevřený',
                  self::STATUS_CLOSED => 'Uzavřený',
                  self::STATUS_FINISHED => 'Odehraný',
                  self::STATUS_CANCELED => 'Zrušený',
                  );
  }
  
  /**
   * Vrací všechny řádky z tabulky.
   * @return Nette\Database\Table\Selection
   */
  public function findAll() {
    return $this->getTable()
                ->select("turnaj.*")
                ->select("uroven.nazev AS uroven_nazev")
                ->select("uroven.akce_id AS akce_id")
                ->select("uroven.akce.nazev AS akce_nazev")
                ->select("uzivatel.krestni AS uzivatel_krestni")
                ->select("uzivatel.prostredni AS uzivatel_prostredni")
                ->select("uzivatel.prijmeni AS uzivatel_prijmeni")
                ->select("uzivatel.login AS uzivatel_login")
                ->select("kraj.nazev AS kraj_nazev")
                ->select("skola.skola_id AS skola_id")
                ->select("skola.nazev AS skola_nazev")
                ->select("skola.mesto AS skola_mesto")
                ->select("kraj:kraj_vazba.region_id AS region_id")
                ->select("kraj:kraj_vazba.region.nazev AS region_nazev")
                ->select("uroven:checklist.checklist_id AS checklist_id")
                ->select("uroven:checklist.sablona AS checklist_sablona")
                ->select(":checklist_stav.stav AS checklist_stav")
                ->order("turnaj.cas, turnaj.turnaj_id");
  }

  /**
   * @return  Nette\Database\Row
   */
  public function findById($turnajId) {
    $result = $this->findAll()->where("turnaj.turnaj_id", $turnajId);
    if (is_array($turnajId)) {
      return $result;
    }
    return $result->fetch();
  }  

  public function getSoupiska() {
    return $this->connection->table(self::SOUPISKA_TABLE)
                            ->select(self::SOUPISKA_TABLE . ".*")
                            ->select('tym.skola_id AS skola_id');  
  }

  public function getSoupiskaRecords($turnajId, $tymId) {
    return $this->getSoupiska()->where(self::SOUPISKA_TABLE . ".turnaj_id", $turnajId)
                                ->where(self::SOUPISKA_TABLE . ".tym_id", $tymId);
  }

  public function getTurnajsByTymId($tymId) {
    $turnajIds = $this->getSoupiska()->where(self::SOUPISKA_TABLE . ".tym_id", $tymId)
                                      ->fetchPairs(null, 'turnaj_id');
    return $this->findAll()->where("turnaj.turnaj_id", $turnajIds);
  }

  public function getVysledky() {
    return $this->connection->table(self::VYSLEDKY_TABLE)
                            ->select(self::VYSLEDKY_TABLE . '.*')
                            ->select('tym.nazev AS tym_nazev')
                            ->select('tym.skola.nazev AS skola_nazev')
                            ->order('poradi');  
  }

  public function getVysledkyRow($turnajId, $tymId) {
    return $this->getVysledky()->where(self::VYSLEDKY_TABLE . ".turnaj_id", $turnajId)
                                ->where(self::VYSLEDKY_TABLE . ".tym_id", $tymId);
  }

  public function getVysledkyByTurnajId($turnajId) {
    return $this->getVysledky()->where(self::VYSLEDKY_TABLE . ".turnaj_id", $turnajId);
  }

  public function insertVysledek($turnajId, $tymId, $poradi) {
    $this->connection->query('INSERT INTO ' . self::VYSLEDKY_TABLE 
                            . '(turnaj_id,tym_id,poradi) VALUES ('
                            . $turnajId . ',' . $tymId . ',' . $poradi . ')' );
  }
  
  public function updateVysledek($turnajId, $tymId, $poradi) {
    $this->connection->query('UPDATE ' . self::VYSLEDKY_TABLE 
                            . ' SET poradi = ' . $poradi . ' WHERE turnaj_id = '
                            . $turnajId . ' AND tym_id = ' . $tymId );
  }
  
  public function deleteVysledek($turnajId, $tymId) {
    $this->connection->query('DELETE FROM ' . self::VYSLEDKY_TABLE 
                            . ' WHERE turnaj_id = ' . $turnajId
                            . ' AND tym_id = ' . $tymId );
  }
  
  public function createGalerie() {
    return $this->connection->table('galerie')->insert(array('verejna' => 0));
  }

  /**
   * @author Petr Vitek
   */     
  public function fetchPossible() {
    return $this->getTable()->order('nazev ASC')->fetchPairs('turnaj_id', 'nazev');
  }

  /**
   * Vrati informace o turnajich pro skoly podle spadovek
   * @author Petr Vitek   
   * @param int $AkceID
   * @param array $SkolyIDs
   * @return Nette\Database\Table\Selection
   */
  public function fetchForMails($AkceID, $SkolyIDs) {
    // select pro poradatelske
    $select = $this->getTable()->select('turnaj.turnaj_id, skola.mesto AS mesto, kraj.nazev AS kraj, turnaj.cas AS datum_cas')
                ->where( 'uroven.akce_id = ?', $AkceID )
                ->where( 'turnaj.skola_id IN ?', $SkolyIDs );
    $select = $this->getTable()
                    ->select('turnaj.turnaj_id')
                    ->select('skola.mesto AS mesto')
                    ->select('kraj.nazev AS kraj')
                    ->select('turnaj.cas AS datum_cas')
                    ->where( 'uroven.akce_id = ?', $AkceID )
                    ->where( 'turnaj.skola_id IN ?', $SkolyIDs );
    $return = $select->fetchPairs( 'turnaj_id');
    
    // select pro skoly ze spadovek
    $select = $this->getTable()->select('turnaj.turnaj_id, skola.mesto AS mesto, kraj.nazev AS kraj, turnaj.cas AS datum_cas')
                ->where( 'uroven.akce_id = ?', $AkceID )
                ->where( ':spadovka.skola_id IN ?', $SkolyIDs );
    $rows = $select->fetchPairs('turnaj_id');
    foreach( $rows as $row ) {
      if ( !isset( $return[ $row->turnaj_id]) ) {
        $return[ $row->turnaj_id ] = $row;
      }
    }
    
    return $return;
  }
  
  /**
   * @author Petr Vitek
   */     
  public function fetchForMailsWithJoins( $AkceID, $SkolyIDs) {
    $return = array();
    // select pro poradatelske
    $select = $this->getTable()->select('turnaj.turnaj_id, skola.mesto AS mesto, kraj.nazev AS kraj, turnaj.cas AS datum_cas')
                ->select( 'uzivatel.krestni AS kk_krestni, uzivatel.prijmeni AS kk_prijmeni, uzivatel.mail AS kk_email, uzivatel.mobil AS kk_mobil')
                ->select( 'skola.nazev AS skola_nazev, skola.mesto AS skola_mesto, skola.ulice AS skola_ulice, skola.psc AS skola_psc')
                ->where( 'uroven.akce_id = ?', $AkceID )
                ->where( 'turnaj.skola_id IN ?', $SkolyIDs );
    $return = $select->fetchPairs( 'turnaj_id');
    
    // select pro skoly ze spadovek
    $select = $this->getTable()->select('turnaj.turnaj_id, skola.mesto AS mesto, kraj.nazev AS kraj, turnaj.cas AS datum_cas')
                ->select( 'uzivatel.krestni AS kk_krestni, uzivatel.prijmeni AS kk_prijmeni, uzivatel.mail AS kk_email, uzivatel.mobil AS kk_mobil')
                ->select( 'skola.nazev AS skola_nazev, skola.mesto AS skola_mesto, skola.ulice AS skola_ulice, skola.psc AS skola_psc')
                ->where( 'uroven.akce_id = ?', $AkceID )
                ->where( ':spadovka.skola_id IN ?', $SkolyIDs );
    $rows = $select->fetchPairs('turnaj_id');
    foreach( $rows as $row ) {
      if ( !isset( $return[ $row->turnaj_id]) ) {
        $return[ $row->turnaj_id ] = $row;
      }
    }
    
    return $return;
  }
  
  /**
   * Vrati pole turnaj_id => [ skola_id]
   * @author Petr Vitek   
   * @param int $AkceID
   * @param array $SkolyIDs
   * @return array 
   */
  public function fetchSpadovkyForSkoly( $AkceID, $SkolyIDs) {
    $return = array();
    // nacte skoly z tabulky spadovek
    $select = $this->connection->table(self::SPADOVKA_TABLE)
                  ->select( 'DISTINCT ' . self::SPADOVKA_TABLE . '.turnaj_id, '
                             . self::SPADOVKA_TABLE . '.skola_id')
                  ->where( 'turnaj.uroven.akce_id = ?', $AkceID )
                  ->where( self::SPADOVKA_TABLE . '.skola_id IN ', $SkolyIDs)
                  ->fetchPairs( 'skola_id', 'turnaj_id');
    
    foreach( $select as $skola_id => $turnaj_id ) {
      if ( !isset( $return[ $turnaj_id] ) ) {
        $return[ $turnaj_id] = array();
      }
      $return[ $turnaj_id][] = $skola_id;
    }
    
    // prida poradatelske pokud jsou v selectu
    $select = $this->getTable()
                    ->select( 'turnaj.turnaj_id, turnaj.skola_id')
                    ->where( 'uroven.akce_id = ?', $AkceID )
                    ->where( 'turnaj.skola_id IN ', $SkolyIDs)
                    ->fetchPairs( 'skola_id', 'turnaj_id');

    foreach( $select as $skola_id => $turnaj_id ) {
      if ( !isset( $return[ $turnaj_id] ) ) {
        $return[ $turnaj_id] = array();
      }
      $return[ $turnaj_id][] = $skola_id;
    }
    return $return;
  }

  /**
   * @author Petr Vitek
   */     
  public function fetchSpadovkyForAkce($AkceID) {
    $return = array();

    $spadove = $this->getTable()->select('uroven:akce.nazev AS akce_nazev, turnaj.turnaj_id, turnaj.cas, skola.nazev AS skola_nazev, skola.mesto')
            ->where('uroven:akce.uroven_id IS NOT NULL')
            // ->where( 'akce.uroven_id = uroven.uroven_id')
            // ->where( 'turnaj.uroven_id = uroven.uroven_id')
            ->order('turnaj.cas ASC');

    if (!is_array($AkceID)) {
      $AkceID = array($AkceID);
    }
    if (count($AkceID) > 0) {
      $AkceID = array_map('intval', $AkceID);
      $spadove->where('akce_id', $AkceID);
    }

    foreach ($spadove as $turnaj) {
      if (!isset($return[$turnaj->akce_nazev])) {
        $return[$turnaj->akce_nazev] = array();
      }
      $date = Nette\Templating\Helpers::date($turnaj->cas, 'm.d');

      $return[$turnaj->akce_nazev][$turnaj->turnaj_id] 
          = "{$turnaj->akce_nazev}.{$date} {$turnaj->mesto} {$turnaj->skola_nazev}";
    }

    return $return;
  }
  
}
