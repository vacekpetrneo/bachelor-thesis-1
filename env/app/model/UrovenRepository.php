<?php

namespace SCG;

class UrovenRepository extends Repository {
  
  /**
   * @author Mask
   */
  public function findAll() {
    return $this->getTable()->select("uroven.*")
                            ->select("akce.rok AS akce_rok")
                            ->select("akce.nazev AS akce_nazev")
                            ->where("uroven.akce_id = akce.akce_id");
  }

  /**
   * @author Mask
   * @param akceId - int or int[]        
   */
  public function getUrovneByAkceId($akceId) {
    return $this->findAll()->where("uroven.akce_id", $akceId);
  }

  /**
   * Najde zaznam podle Id
   * @author Mask   
   * @param int $Id
   * @return \Nette\Database\Table\ActiveRow | FALSE
   */
  public function findById($Id) {
    return $this->findAll()->where("uroven.uroven_id", $Id)->fetch();
  }
  
}