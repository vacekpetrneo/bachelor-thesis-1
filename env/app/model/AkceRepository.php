<?php

namespace SCG;

class AkceRepository extends Repository {

	const TYP_SCG = 0;
	const TYP_PISKVORKY = 1;
	const TYP_PREZENTIADA = 2;
	const TYP_AKADEMIE = 3;  
  
	/**
	 * @return NRow
	 */
	public function findById($AkceID) {
		return $this->findBy(array('akce_id' => $AkceID ))->fetch();
	}

	public static function canonName( $nazev ) {
		return preg_replace( '/(.*)\s+\d{1,4}\s*$/', '\1', $nazev, 1);
	}
	
	/**
	 * Vrati pole pro select box s akcemi z db
   * @param boolean $withoutTypes Default false - pouzije typy akci v poli, true - ciste pole id=>akce
	 */
	public function fetchPossible( $withoutTypes = false) {
		$opts = array();
		$akce = $this->findAll()->select( 'akce_id, nazev')->order( 'typ ASC, rok ASC');
		foreach( $akce as $item ) {
      if ( $withoutTypes ) {
        // ciste pole
        $opts[ $item->akce_id] = $item->nazev;
      } else {
        // typy akci pro optgroup
        $baseName = self::canonName( $item->nazev );
        if ( !isset( $opts[ $baseName] ) ) {
          $opts[ $baseName] = array();
        }
        $opts[ $baseName][ $item->akce_id] = $item->nazev;
      }
		}
		return $opts;
	}
  
  public function fetchRunning() {
    return $this->findAll()->where("bezi = 1");
  }

  public function translateAkceTypByCode($code) {
  	$akce = fetchPossibleTypes();
  	if ( isset( $akce[$code])) {
  		return $akce[$code];
  	}
      
  	return 'Neznámý typ akce';     
  }
  
  public function fetchPossibleTypes() {
    return array( self::TYP_SCG => 'Student Cyber Games',
                    self::TYP_PISKVORKY => 'pIšQworky',
                    self::TYP_PREZENTIADA => 'Prezentiada',
                    self::TYP_AKADEMIE => 'Akademie');
  }
  
}
