<?php

namespace SCG;

use Nette\Diagnostics\Logger;

/**
 * Trida pro praci s pripravenymi materialy pro maily spadovkam
 *
 * @author rattus
 */
class MailZipArchive {

  protected $zip = FALSE;

  public function __construct() {
    
  }

  /**
   * Otevre zip soubor
   * @param string $Path
   * @param string $fileName
   * @throws MailZipArchiveException
   */
  public function openZipFile($Path, $fileName) {
    $this->zip = new \ZipArchive();
    $opened = $this->zip->open($Path . $fileName);
    if ($opened !== TRUE) {
      throw new MailZipArchiveException("Chyba otevírání souboru {$fileName} - " . $opened);
    }
  }

  /**
   * Vrati ID skol
   * @return array
   * @throws MailZipArchiveException
   */
  public function getSkoly() {
    if ($this->zip === FALSE) {
      throw new MailZipArchiveException("Není otevřen žádný zip.");
    }
    $comment = $this->zip->getArchiveComment();
    $commentParts = $this->parseComment($comment);
    return explode(',', $commentParts['skoly']);
  }

  public function getPrilohyProTurnaj($turnajID) {
    if ($this->zip === FALSE) {
      throw new MailZipArchiveException("Není otevřen žádný zip.");
    }

    $files = array();
    for ($i = 0; $i < $this->zip->numFiles; $i++) {
      $filename = $this->zip->getNameIndex($i);
      if (substr($filename, -1) == '/') {
        // skip directory entries
        continue;
      }
      if ($this->getTurnajFromFilename($filename) == $turnajID) {
        $files[] = $filename;
      }
    }

    return $files;
  }

  public function extractToTemp($tmpPath, $Files) {
    if ($this->zip === FALSE) {
      throw new MailZipArchiveException("Není otevřen žádný zip.");
    }
    @mkdir($tmpPath);
    $this->zip->extractTo($tmpPath, $Files);

    $extracted = array();
    foreach ($Files as $file) {
      $extracted[] = $tmpPath . $file;
    }
    return $extracted;
  }

  public function deleteTemp($extractedFiles) {
    foreach( $extractedFiles as $file) {
      unlink($file);
    }
    rmdir( dirname( $extractedFiles[0]) );
  }

  public function createZipTemplate($Path, \Nette\Security\Identity $User, $turnaje, $skoly, $selectText) {

    $date = new \Nette\DateTime();
    $zipFileName = $Path . $date->format('Y-m-d_H-i') . '_' . $User->login . '.zip';

    $zip = new \ZipArchive();
    $opened = $zip->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
    if ($opened !== TRUE) {
      throw new MailZipArchiveException($opened);
      return FALSE;
    }
    foreach ($turnaje as $turnaj) {
      $zip->addEmptyDir(StringUtils::friendlyUrl($turnaj->kraj) . '_' . StringUtils::friendlyUrl($turnaj->mesto) . '_' . $turnaj->turnaj_id);
    }
    $zip->setArchiveComment(implode(',', $skoly) . ';' . $selectText);
    $zip->close();

    return $zipFileName;
  }

  public function getPreparedTemplates($Path, Logger $logger) {
    $archives = array();

    $zip = new \ZipArchive();
    $dir = dir($Path);
    while (false !== ($entry = $dir->read())) {
      if (substr($entry, -4) != '.zip') {
        // skip non zips
        continue;
      }
      $opened = $zip->open($Path . $entry);
      if ($opened !== TRUE) {
        if ($logger !== NULL) {
          $logger->log("Chyba otevírání souboru {$entry} - " . $opened, Logger::WARNING);
        }
        continue;
      }
      $comment = $zip->getArchiveComment();
      $files = array();
      for ($i = 0; $i < $zip->numFiles; $i++) {
        $files[] = $zip->getNameIndex($i);
      }
      $zip->close();
      $fileParts = $this->parseFileName($entry);
      $commentParts = $this->parseComment($comment);

      $archives[] = array(
          'filename' => basename($entry),
          'date' => $fileParts['date'],
          'user' => $fileParts['user'],
          'skoly' => $commentParts['skoly'],
          'select' => $commentParts['select'],
          'files' => $files,
      );
    }
    $dir->close();

    return $archives;
  }

  protected function parseFileName($fileName) {
    $parts = explode("_", basename($fileName));
    $userParts = explode('.', $parts[2]);
    return array('date' => $parts[0] . ' ' . str_replace('-', ':', $parts[1]), 'user' => $userParts[0]);
  }

  protected function parseComment($commentString) {
    $parts = explode(';', $commentString, 2);
    return array('skoly' => $parts[0], 'select' => $parts[1]);
  }

  protected function getTurnajFromFilename($fileName) {
    return substr(strrchr(dirname($fileName), '_'), 1);
  }

}

class MailZipArchiveException extends \Exception {
  
}
