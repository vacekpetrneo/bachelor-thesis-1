<?php

namespace SCG;

use Nette;

class RegionTree {
	
	protected $_Regiony = false;
	protected $_Kraje = false;
	/**
	 * id hlavniho regionu ( vsechny oblasti )
	 * @var int
	 */
	protected $_MainRegion = false;
	protected $_KrajeFiltr = false;
	
	protected $_allFromParent = false;
	
	/** @var \Nette\Database\Context */
	protected $connection;
	
	public function __construct( Nette\Database\Context $connection) {
		$this->connection = $connection;
		
		if ( $this->_Regiony === false ) {
			$regiony = $this->connection->table( 'region')->order('nazev ASC');
			$this->_Regiony = array();
			foreach( $regiony as $region ) {
				if ( $region->parent_id == 0 ) {
					$this->_MainRegion = $region->region_id;
				}
				$this->_Regiony[ $region->region_id] = array(
					'data' => $region,
					'vazby' => array(),
				);
			}

			$kraje = $this->connection->table( 'kraj')->order('nazev ASC');
			$this->_Kraje = array();
			foreach( $kraje as $kraj ) {
				$this->_Kraje[ $kraj->kraj_id] = array(
					'data' => $kraj,
					'vazby' => array(),
				);
			}

			$vazby = $this->connection->table( 'kraj_vazba');
			foreach( $vazby as $vazba ) {
				$this->_Regiony[ $vazba->region_id]['vazby'][] = $vazba->kraj_id;
				$this->_Kraje[ $vazba->kraj_id]['vazby'][] = $vazba->region_id;
			}
		}
		
	}
	
	/**
	 * Nastavi filtr ktere kraje vypisovat. 
	 * @param array $kraje Pole kraju, pouziji se jen klice - kraj_id
	 */
	public function filterKraje( $kraje ) {
		if ( !is_array($kraje) ) {
			return FALSE;
		}
		
		$this->_KrajeFiltr = array_flip( array_keys( $kraje ) );
		return true;
	}
	
	public function clearFilter() {
		$this->_KrajeFiltr = false;
	}
	
	/**
	 * Nastavi mod zobrazeni vsech kraju patrici pod region -> 16 CR, 17 SR
	 * @param int $regionID 
	 */
	public function setAllChildsRegion( $regionID ) {
	    $this->_allFromParent = $regionID;
	}
	
  public function getRegionyArray() {
    $result = array();
    
    // $result = $this->getAllChildren( $this->getRegionById( $this->_MainRegion) );
    
    foreach( $this->_Regiony as $region_id => $regionAr ) {
      $result[ $region_id] = $regionAr['data']->nazev;
    }
    
    \Nette\Diagnostics\Debugger::barDump( $result, "regiony" );
    
    return $result;
    /*
    foreach( $regiony as $region_id => $regionAr) {
      
    }
     */
  }
  
	/**
	 * @return Pole pro select kraju, republiky oddelene option grupou 
	 */
	public function getOptionsArray() {
		$result = array();
		
		foreach( $this->_Regiony as $region_id => $regionAr ) {
			if ( $regionAr['data']->parent_id == $this->_MainRegion ) {
				$result[ $regionAr['data']->nazev] = array();
				foreach( $regionAr['vazby'] as $kraj_id ) {
					if ( $this->_KrajeFiltr === false || 
					     isset( $this->_KrajeFiltr[ $kraj_id] ) ||
					     ( $this->_allFromParent !== false && $this->_allFromParent == $region_id ) ) {
						// neni zadny filtr, nebo kraj je ve filtru
						$result[ $regionAr['data']->nazev][ $kraj_id ] = $this->_Kraje[ $kraj_id]['data']->nazev;
					}
				}
			}
		}
		
		return $result;
		
	}
	
	/**
	 * @return region jako asociativní pole
	 */
	public function getRegionById($id) {
    if(!empty($id) && is_int($id))
      return $this->_Regiony[$id]['data']->toArray();    
    return null;
  } 

	/**
	 * @return Předek daného regionu
	 */
	public function getParent($region) {
    $result = null;
    if(   is_array($region)
       && isset($region['parent_id'])
       && is_int($region['parent_id'])
       ) 
      foreach($this->_Regiony as $supp) {
        if ($supp['data']->region_id == $region['parent_id']) {
          $result = $supp['data']->toArray();
          break;
        }      
      }
    return $result;    
  }

	/**
	 * @return Přímí potomci daného regionu
	 */
	public function getChildren($region) {
    $result = array();
    if(   is_array($region)
       && isset($region['region_id'])
       && is_int($region['region_id'])
       )
    {
      foreach($this->_Regiony as $supp) {
        if ($supp['data']->parent_id == $region['region_id']) {
          $result[$supp['data']->region_id] = $supp['data']->toArray();
        }
      }
    }
    return $result;
  }

	/**
	 * @return Všichni potomci daného regionu
	 */
  public function getAllChildren($region, $level = 0) {
    $supp = $this->getChildren($region);
    $result = array();
    foreach ($supp as $subregion) {
      $subregion['level'] = $level;
      $result[$subregion['region_id']] = $subregion;
      $result = $result + $this->getAllChildren($subregion, $level+1);
    }
    return $result;   
  }

}
