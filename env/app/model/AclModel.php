<?php

namespace SCG;

use Nette, Nette\Caching\Cache;

/**
 * Loads ACL from database
 *
 * @author rattus
 */
class AclModel extends Nette\Object {

	/** @var Nette\Database\Context */
	protected $connection;

	/** @var Nette\Caching\Cache */
	protected $cache = null;

	const ACL_TABLE = 'pravo_skupiny';
	const PRIVILEGES_TABLE = 'pravo';
	const RESOURCES_TABLE = 'region_opravneni';
	const ROLES_TABLE = 'skupina';

	public function __construct(Nette\Database\Context $connection) {
		$this->connection = $connection;
	}

	/**
	 * Sets cache storage engine
	 * @param  Nette\Caching\IStorage $storage
	 * @return AclModel   provides a fluent interface
	 */
	public function setCacheStorage( Nette\Caching\IStorage $storage = NULL) {
		$this->cache = $storage ? new Nette\Caching\Cache($storage, 'AclModel') : NULL;
		return $this;
	}

	public function getRoles() {
		$roles = $this->cache !== null ? $this->cache->load('roles') : null;
		if ($roles === null) {
			$roles = array();
			foreach ($this->connection->table(self::ROLES_TABLE) as $role) {
				$roles[$role['skupina_id']] = array('name' => $role->nazev,
					// 'parent_id' => $role->parent_id,
					// 'parent_name' => ( $role->ref( self::ROLES_TABLE, 'parent_id' ) == null ) ? null : $role->ref( self::ROLES_TABLE, 'parent_id' )->name,
					'parent_name' => null,
				);
			}

			if ($this->cache !== null) {
				$this->cache->save('roles', $roles, array(
					Cache::EXPIRE => '+ 30 minutes',
				));
			}
		}
		return $roles;
	}

	public function getResources() {
		// FIXME : return regiony, turnaje
		return array();

		$resources = $this->cache !== null ? $this->cache->load('resources') : null;
		if ($resources === null) {
			$resources = array();
			foreach ($this->connection->table(self::RESOURCES_TABLE) as $resource) {
				$resources[$resource['id']] = array('name' => $resource->name);
			}

			if ($this->cache !== null) {
				$this->cache->save('resources', $resources, array(
					Cache::EXPIRE => '+ 30 minutes',
				));
			}
		}
		return $resources;
	}

	public function getPrivileges() {
		$privileges = $this->cache !== null ? $this->cache->load('privileges') : null;
		if ($privileges === null) {
			$privileges = $this->connection->table(self::PRIVILEGES_TABLE)->fetchPairs('pravo_id', 'nazev');
			if ($this->cache !== null) {
				$this->cache->save('privileges', $privileges, array(
					Cache::EXPIRE => '+ 30 minutes',
				));
			}
		}
		return $privileges;
	}
	
	public function getRules() {
		$rules = $this->cache !== null ? $this->cache->load('rules') : null;
		if ($rules === null) {
			$rules = array();
			foreach ($this->connection->table(self::ACL_TABLE)
					->select(self::ROLES_TABLE . '.nazev AS role_name')
					// ->select( self::RESOURCES_TABLE. '.name AS resource_name')
					->select(self::PRIVILEGES_TABLE . '.nazev AS privilege_name')
			as $rule) {
				$rules[] = array(
					'allowed' => 1,
					'role' => $rule->role_name,
					//'resource' => $rule->resource_name,
					'resource' => Nette\Security\IAuthorizator::ALL,
					'privilege' => $rule->privilege_name,
				);
			}

			if ($this->cache !== null) {
				$this->cache->save('rules', $rules, array(
					Cache::EXPIRE => '+ 30 minutes',
				));
			}
		}
		return $rules;
	}

}
