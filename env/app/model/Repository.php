<?php

namespace SCG;

use Nette;

/**
 * Provádí operace nad databázovou tabulkou.
 */
abstract class Repository extends Nette\Object {

	/** @var Nette\Database\Context */
	protected $connection;

	public function __construct(Nette\Database\Context $db) {
		$this->connection = $db;
	}

	protected function getTableName() {
		// název tabulky odvodíme z názvu třídy
		preg_match('#(\w+)Repository$#', get_class($this), $m);

		if (function_exists('lcfirst') === false) {
			$m[1][0] = strtolower($m[1][0]);
		} else {
			$m[1] = lcfirst($m[1]);
		}
		return $m[1];
	}

	/**
	 * Vrací objekt reprezentující databázovou tabulku.
	 * @return Nette\Database\Table\Selection
	 */
	protected function getTable() {
		return $this->connection->table($this->getTableName());
	}

	/**
	 * Vrací všechny řádky z tabulky.
	 * @return Nette\Database\Table\Selection
	 */
	public function findAll() {
		return $this->getTable();
	}

	/**
	 * Vrací řádky podle filtru, např. array('name' => 'John').
	 * @return Nette\Database\Table\Selection
	 */
	public function findBy(array $by) {
		return $this->getTable()->where($by);
	}

}