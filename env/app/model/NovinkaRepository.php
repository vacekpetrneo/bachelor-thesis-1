<?php

namespace SCG;

use Nette;

class NovinkaRepository extends Repository {

  /**
   * Vrati novinky pro danou akci
   * @param int $akceID
   * @return Nette\Database\Table\Selection
   */
  public function findAllByAkce( $akceID ) {
    return $this->findBy( array( 'akce_id' => $akceID ))->order( 'cas DESC' );
  }
  
	/**
	 * @return  Nette\Database\Row
	 */
	public function findById($NovinkaID) {
		return $this->findBy(array('novinka_id' => $NovinkaID ))->fetch();
	}
	
	public function fetchPossible() {
		return $this->getTable()->order('nazev ASC')->fetchPairs( 'turnaj_id', 'nazev');
	}
	
	
	public function fetchPossibleKategorie() {
    return $this->connection->table( 'kategorie')->order( 'nazev ASC')->fetchPairs( 'kategorie_id', 'nazev');
	}

  /**
   * Vytvori novou novinku podle dat z formulare
   * @param array $formValues
   * @return \Nette\Database\Table\ActiveRow nove vytvoreny zaznam
   */
  public function insert( array $formValues ) {
    $this->validateInsert( $formValues );
    $formValues[ 'publikovano'] = 0;
    
    try {
      $this->connection->beginTransaction();
      
      $galerieRow = $this->connection->table( 'galerie' )->insert( array( 'verejna' => 0 ) );
      $diskuzeRow = $this->connection->table( 'diskuze' )->insert( array( 'schovat' => 0) );
      
      $formValues['galerie_id'] = $galerieRow->galerie_id;
      $formValues['diskuze_id'] = $diskuzeRow->diskuze_id;
      
      $row = $this->getTable()->insert( $formValues );
      
      $this->connection->commit();
    } catch( Exception $e ) {
      $this->connection->rollBack();
      throw $e;
    }
    
    if ( $row === false ) {
      throw new Exception( implode( ' : ', $this->connection->errorInfo() ) );
    }
    return $row;
  }
  
  public function validateInsert( array $data ) {
    
    return true;
  }
  
}
