<?php

namespace SCG;

class UserRepository extends Repository {

    private $userSettings = array();

    /**
     * Najde zaznam podle ID
     * @param int $ID
     * @return \Nette\Database\Table\ActiveRow | FALSE
     */
    public function findById($ID) {
        return $this->findBy(array('uzivatel_id' => $ID))->fetch();
    }

    /**
     * @return string
     */
    protected function getTableName() {
        return 'uzivatel';
    }

    /**
     * @return  Nette\Database\Row
     */
    public function findByLogin($username) {
        return $this->findBy(array('login' => $username))->fetch();
    }

    /**
     * 
     * @param type $userID
     * @return array
     */
    public function getUserRoles($userID) {
        $roles = $this->connection->table('opravneni')
                ->select('akce_id')
                ->select('skupina.nazev AS skupina')
                ->where('uzivatel_id = ?', $userID);

        $rolesArr = array();
        foreach ($roles as $role) {
            if (!isset($rolesArr[$role->akce_id]))
                $rolesArr[$role->akce_id] = array();
            $rolesArr[$role->akce_id][] = $role->skupina;
        };
        return $rolesArr;
    }

    /**
     * Computes salted password hash.
     * @param  string
     * @return string
     */
    public function calculateHash($password, $salt = '') {
        return hash('sha256', $password . $salt);
        // return crypt($password, ($tmp=$salt) ? $tmp : '$2a$07$' . NStrings::random(22));
    }

    /**
     * Generovani nahodneho hashe
     * @return string
     */
    protected function generateHash() {
        $fstat = '';

        $fp = @fopen(__FILE__, 'r');
        if ($fp !== FALSE) {
            $fstat = @implode('', @fstat($fp));
            @fclose($fp);
        }

        return md5(
                microtime() .
                uniqid(mt_rand(), true) .
                $fstat
        );
    }

    /**
     * Nastavi nove heslo uzivateli
     * @param int $id
     * @param string $Password Nove heslo
     * @return 1 | FALSE
     * @throws Exception
     */
    public function setNewPassword($id, $Password) {
        $salt = substr($this->generateHash(), 0, 20);
        $hashedPassword = $this->calculateHash($Password, $salt);

        $userRow = $this->findById($id);
        if ($userRow === FALSE) {
            throw new Exception("Uživatel nenalezen");
        }

        return $userRow->update(array(
                    'pass' => $hashedPassword,
                    'salt' => $salt,
        ));
    }

    public function getAkceSetting($userId) {
        return $this->_getSetting($userId, 'akce_id');
    }
    
    public function getPrimaryAkceSetting( $userId) {
        $akce = $this->getAkceSetting( $userId );
        if ( $akce === NULL ) {
          return NULL;
        }
        return reset( $akce );
    }

    public function setAkceSetting($userId, $akceIDs) {
        return $this->_setSetting($userId, 'akce_id', $akceIDs);
    }

    private function _setSetting($userId, $settingName, $value) {
        $user = $this->findBy(array('uzivatel_id' => $userId))->select('uzivatel_id,settings')->fetch();
        if ($user === FALSE) {
            throw new \InvalidArgumentException("UserID not found in DB");
        }
        if (!isset($user['settings'])) {
            $settings = array();
        } else {
            $settings = json_decode($user['settings'], TRUE);
        }
        $settings[$settingName] = $value;
        return $user->update(array('settings' => json_encode($settings)));
    }

    private function _getSetting($userId, $settingName, $defaultValue = NULL, $useCache = TRUE) {
        if (!isset($this->userSettings[$userId])) {
            $user = $this->findBy(array('uzivatel_id' => $userId))->select('settings')->fetch();
            if ($user === FALSE) {
                throw new \InvalidArgumentException("UserID not found in DB");
            }
            $this->userSettings[ $userId] = $user;
        } else {
            $user = $this->userSettings[ $userId];
        }

        if (!isset($user['settings'])) {
            return $defaultValue;
        }


        $settings = json_decode($user['settings'], TRUE);
        if (!isset($settings[$settingName])) {
            return $defaultValue;
        }
        return $settings[$settingName];
    }

}
