<?php

namespace SCG;

class OkresRepository extends Repository {

	/**
	 * @return Nette\Database\Row
	 */
	public function findById($OkresID) {
		return $this->findBy(array('okres_id' => $OkresID ))->fetch();
	}
	
	/**
	 * Vrati pole okresu, serazene podle nazvu
	 * @return array
	 */
	public function fetchPossible() {
		return $this->getTable()->order('nazev ASC')->fetchPairs( 'okres_id', 'nazev');
	}

}
