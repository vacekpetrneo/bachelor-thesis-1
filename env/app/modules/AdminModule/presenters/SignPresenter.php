<?php

namespace AdminModule;

use Nette\Security\AuthenticationException,
    Nette\Application\UI\Form;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter {

  /** @persistent */
  public $backlink = '';

  protected function startup() {
    parent::startup();
    $this->context->httpResponse->setHeader('Pragma', 'no-cache');
  }

  /**
   * Sign-in form factory.
   * @return NAppForm
   */
  protected function createComponentSignInForm() {
    $form = new Form;
    
    $form->getElementPrototype()->class = 'pure-form';
    
    $form->addText('username', 'Login:')
            ->setRequired('Vložte své jméno.');

    $form->addPassword('password', 'Heslo:')
            ->setRequired('Vložte své heslo.');

    $form->addCheckbox('remember', 'Pamatovat přihlášení');

    $form->addProtection('Vypršel časový limit, odešlete prosím formulář znovu.', 300);

    $form->addSubmit('login', 'Login')->setAttribute('class', 'pure-button pure-button-primary');

    // call method signInFormSucceeded() on success
    $form->onSuccess[] = $this->signInFormSucceeded;
    return $form;
  }

  public function signInFormSucceeded(Form $form) {
    $values = $form->getValues();

    if ($values->remember) {
      // $this->getUser()->setExpiration('14 days', FALSE);
      // FIXME : savana nepodporuje vic jak 18 hodin ci co... :-/
      $this->getUser()->setExpiration('18 hours', FALSE);
    } else {
      $this->getUser()->setExpiration('60 minutes', TRUE);
    }

    try {
      $this->getUser()->login($values->username, $values->password);
      $this->flashMessage('Přihlášení proběhlo úspěšně', 'success');
      $this->restoreRequest($this->backlink);
      $this->redirect('Dashboard:');
    } catch (AuthenticationException $e) {
      $form->addError($e->getMessage());
    }
  }

  public function actionOut() {
    $this->getUser()->logout();
    $this->flashMessage('Byli jste odhlášeni');
    $this->redirect('in');
  }

  protected function createComponentPasswordForm() {
    $form = new Form();
    $form->getElementPrototype()->class = 'pure-form';
    $form->addPassword('oldPassword', 'Staré heslo:', 30)
            ->addRule(Form::FILLED, 'Je nutné zadat staré heslo.');
    $form->addPassword('newPassword', 'Nové heslo:', 30)
            ->addRule(Form::MIN_LENGTH, 'Nové heslo musí mít alespoň %d znaků.', 6);
    $form->addPassword('confirmPassword', 'Potvrzení hesla:', 30)
            ->addRule(Form::FILLED, 'Nové heslo je nutné zadat ještě jednou pro potvrzení.')
            ->addRule(Form::EQUAL, 'Zadná hesla se musejí shodovat.', $form['newPassword']);
    $form->addSubmit('set', 'Změnit heslo')->setAttribute('class', 'pure-button pure-button-primary');
    $form->onSuccess[] = callback( $this, 'passwordFormSubmitted' );
    return $form;
  }

  public function passwordFormSubmitted(Form $form) {
    $values = $form->getValues();
    $user = $this->getUser();
    $authenticator = $user->getAuthenticator();
    try {
      $authenticator->authenticate(array($user->getIdentity()->login, $values['oldPassword']));
      $authenticator->changePassword( $user->getId(), $values['newPassword']);
      $this->flashMessage('Heslo bylo změněno.', 'success');
      $this->redirect('Dashboard:default');
    } catch ( AuthenticationException $e) {
      $form->addError('Zadané heslo není správné.');
    }
  }

}
