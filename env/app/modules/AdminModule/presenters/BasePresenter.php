<?php

namespace AdminModule;

use Nette\Security\IAuthorizator, Nette\Security\IUserStorage;
use Nette\Diagnostics\Debugger;

/**
 * Parent of all admin module presenters
 *
 * @author rattus
 */
abstract class BasePresenter extends \SCG\BasePresenter {

	/** @var AdminMenu */
	protected $menu;
	
	/** @var int Aktualne zvolena akce */
	protected $currentAkceID;
	
	/** @var string Aktualni db */
	protected $currentDB;
  
	/** @var int Aktualne prihlaseny uzivatel */
	protected $currentUserId;
  
  /** @var \SCG\MassMailer */
  protected $massMailer;
	
	/**
	 * (non-phpDoc)
	 *
	 * @see NPresenter#startup()
	 */
	protected function startup() {
		parent::startup();

    $this->showDBFlashMessage();
		
		// if not on signup/error page
		if ($this->name != 'Admin:Sign' && $this->name != 'Admin:Error') {
			if ($this->getUser()->isLoggedIn()) {
        $this->currentUserId = $this->getUser()->getIdentity()->id;
        $this->currentAkceID = $this->userRepo->getPrimaryAkceSetting($this->currentUserId);
      }
      else {
				if ($this->user->logoutReason === IUserStorage::INACTIVITY) {
					$this->flashMessage('Byli jste odhlášeni z důvodu neaktivity. Prosím přihlašte se znovu.');
				}
				
				if ($this->isAjax()) { // AJAX request? Just note this error in payload.
					$this->payload->error = TRUE;
					$this->terminate();
				} else {
					$this->redirect('Sign:in', array('backlink' => $this->storeRequest()));
				}
			}
		}

		
		$this->menu = new AdminMenu( $this, 'adminMenu' );
		
	}
	
  public function injectMassMailer( \SCG\MassMailer $mailer ) {
    $this->massMailer = $mailer;
  }
  
	/**
	 * Zkontroluje opravneni prihlaseneho uzivatele pro dane pravo v dane akci
	 * Pokud se misto id akce zada primo pravo, zkontroluje se zda ma uzivatel dane opravneni alespon v jedne akci 
	 * 
	 * @param int|string $AkceID ID akce, pokud je string - zkontroluji se vsechny akce
	 * @param string|null $Privilege Jmeno prava, null pokud se hleda ve vsech akcich
	 * @param boolean $RedirectOnError Default chovani presmeruje na error presenter, da se potlacit nastavenim na false
	 * @return boolean true pokud uzivatel ma pravo, jinak false
	 */
	protected function checkRights( $AkceID, $Privilege = null, $RedirectOnError = true ) {
		$user = $this->getUser();
		
		// pokud je akceid string a privilege neuvedeno, vezmi akci jako privilege
		if ( !is_numeric( $AkceID ) && $Privilege === null ) {
			$Privilege = $AkceID;
			$AkceID = null;
		}
		
		if ( $AkceID == null ) {
			// user ma pravo pro alespon jednu akci
			if ( $user->isAllowedForSomeAkce( IAuthorizator::ALL, $Privilege) ) {
				return true;
			}
		} else {
			// user ma pravo v dane akci
			if ( $user->isAllowedForAkce( $AkceID, IAuthorizator::ALL, $Privilege) ) {
				return true;
			}
		}
		
		if ( $RedirectOnError ) {
			$this->redirect('Error:auth', array('Privilege' => $Privilege, 'Akce' => $AkceID ) );
		}
		return false;
	}

  public function getCurrentDB() {
    return $this->currentDB;
  }
  
  private function showDBFlashMessage() {
    
    $params = $this->context->getParameters();
    $this->currentDB = $params['database']['dbname'];
    
    return;
    
    $showDB = false;
    if ( !$this->hasFlashSession() ) {
      $showDB = true;
    } else {
      $flashSess = $this->getFlashSession();
      if ( !isset( $flashSess['dbmessage'] ) ) {
        $showDB = true;
      }
    }
    if ( $showDB ) {
      if ( $this->currentDB == 'scg_akce_devel' ) {
        $this->flashMessage( "Databáze DEVELOPMENT - {$this->currentDB}", 'info' );
      } else {
        $this->flashMessage( "Databáze PRODUKCNI - {$this->currentDB}", 'warning' );
      }
      $flashSess['dbmessage'] = true;
    }
  }
  
  public function createComponentAkceSelektor() {
      $control = new AkceSelektor();
      $control->setAkceRepo( $this->akceRepo );
      $control->setUserRepo( $this->userRepo );
      return $control;
  }
  
}