<?php

namespace AdminModule;

use Nette, Nette\Application\UI\Form, Nette\Diagnostics\Debugger;

/**
 * @author Mask
 */
class OsobaPresenter extends BasePresenter {

  protected $osoba = null;
  protected $osoby = array();

	/**
	 * (non-phpDoc)
	 *
	 * @see Nette\Application\Presenter#startup()
	 */
	protected function startup() {
		parent::startup();
    switch ($this->getAction) {
      case 'default' : 		
      case 'view' :
        $this->checkRights('KONTAKTNIOSOBY-READ' );
        break;
      case 'create' :
      case 'edit' :
        $this->checkRights('KONTAKTNIOSOBY-WRITE');
        break;
    }
	}
  
  public function searchFormSubmitted(Nette\Forms\Controls\SubmitButton $submit) {
    $form = $submit->getForm();
    $values = $form->getValues();
    
    foreach($values as $key => $value) {
      if (is_string($value))
        $values[$key] = strtolower($value);
    }
    
    $osoby = $this->osobaRepo->findAll();      

    if (isset($values['pohlavi'])) {
      if ($values['pohlavi']!=2) {
        $osoby->where('pohlavi = ?', $values['pohlavi']);
      }
    }

    if (isset($values['jmeno']) && !empty($values['jmeno'])) {
      $jmeno = preg_split("/ /", $values['jmeno'], null, PREG_SPLIT_NO_EMPTY);
      $columnName = 'CONCAT(LOWER(titul_pred)," ",LOWER(krestni)," ",LOWER(prostredni)," ",LOWER(prijmeni)," ",LOWER(titul_za))';
      $osoby->where($columnName . " LIKE ?", '%' . reset($jmeno) . '%');
      $i = next($jmeno);
      while($i) {
        $osoby->where($columnName . " LIKE ?", '%' . $i . '%');
        $i = next($jmeno);
      }
    }

    if (isset($values['email']) && !empty($values['email'])) {
      $osoby->where('LOWER(email) LIKE ?', '%' . $values['email'] . '%');  
    }

    if (isset($values['telefon']) && !empty($values['telefon'])) {
      $osoby->where('telefon LIKE ?', '%' . $values['telefon'] . '%');  
    }

    if (isset($values['mobil']) && !empty($values['mobil'])) {
      $osoby->where('mobil LIKE ?', '%' . $values['mobil'] . '%');  
    }

    if (isset($values['icq']) && !empty($values['icq'])) {
      $osoby->where('icq LIKE ?', '%' . $values['icq'] . '%');  
    }

    if (isset($values['skype']) && !empty($values['skype'])) {
      $osoby->where('LOWER(skype) LIKE ?', '%' . $values['skype'] . '%');  
    }

    if (isset($values['skola_id']) && !empty($values['skola_id'])) {
      $osoby->where('skola_id LIKE ?', '%' . $values['skola_id'] . '%');  
    }

    if (isset($values['pozice']) && !empty($values['pozice'])) {
      $osoby->where('LOWER(pozice) LIKE ?', '%' . $values['pozice'] . '%');  
    }

    if (isset($values['primarni']) && $values['primarni']!=2) {
      $osoby->where('primarni = ?', $values['primarni']);
    }

    if (isset($values['osoba_typ']) && !empty($values['osoba_typ'])) {
      $osoby->where('kontaktni_osoba.kontaktni_osoba_typ_id', $values['osoba_typ']);    
    }

    if (isset($values['akce_typ']) && !empty($values['akce_typ'])) {
      $supp = $this->osobaRepo->getAllOsobyAkce()
                              ->select('kontaktni_osoba_id')
                              ->where('akce_typ', $values['akce_typ']);
      $osoby->where('kontaktni_osoba.kontaktni_osoba_id', $supp);    
    }

    if (isset($values['region']) && !empty($values['region'])) {
      $supp = array();
      foreach($values['region'] as $region) {
        $supp += array_keys($this->regionTree
                                  ->getAllChildren(
                                      array('region_id' => (int)$region)));
      }
      $values['region'] = array_merge($values['region'], $supp);
      $supp = $this->skolaRepo->findAll()
                              ->select('skola_id')
                              ->where('region_id', $values['region'])
                              ->fetchPairs(null, 'skola_id');
      $osoby->where('skola_id', $supp);    
    }

    $this->osoby = $osoby;        
  }

	/**
	 * Osoba search form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentOsobaSearchForm() {	
		$form = new Nette\Application\UI\Form;

    $form->getElementPrototype()->class = 'pure-form';
    $form->addGroup('Hledat osoby');

    $form->addRadioList('pohlavi', 'Pohlaví', array('žena', 'muž', 'oboje'))
          ->setDefaultValue(2);
    $form->addText('jmeno', 'Jméno osoby', 80);
    $form->addText('email', 'E-mail', 80);
    $form->addText('telefon', 'Telefon', 80);
    $form->addText('mobil', 'Mobil', 80);
    $form->addText('icq', 'ICQ', 80);
    $form->addText('skype', 'Skype', 80);
    $form->addText('skola_id', 'ID školy osoby', 80);
    $form->addText('pozice', 'Pozice', 80);
    $form->addRadioList('primarni', 'Primární', array('Ne', 'Ano', 'oboje'))
          ->setDefaultValue(2);

    //typ
    $osobaTypes = $this->osobaRepo->fetchPossibleTypes(); 
    $form->addMultiSelect('osoba_typ', 'Typ osoby', $osobaTypes, 1);

    //akce
    $akceTypes = $this->akceRepo->fetchPossibleTypes();    
    $form->addMultiSelect('akce_typ', 'Typ akcí', $akceTypes, 1);

    //region
    $republiky = $this->regionTree->getChildren($this->regionTree->getRegionById(1));
    $regions = array();
    foreach ($republiky as $republika) {
      $regions['Státy'][$republika['region_id']] = $republika['nazev'];
      $kraje = $this->regionTree->getChildren($republika);
      foreach ($kraje as $kraj) {
        $regions[$kraj['nazev']][$kraj['region_id']] = $kraj['nazev'];
        $okresy = $this->regionTree->getChildren($kraj);
        foreach ($okresy as $okres) {
          $regions[$kraj['nazev']][$okres['region_id']] = $okres['nazev'];
        }
      }
    }
    $form->addMultiSelect('region', 'Region', $regions, 1);

    $form->addSubmit('search', 'Hledat')->onClick[]
                     = array($this, 'searchFormSubmitted');
    
    return $form;
		
	}
  
	public function renderDefault() {      
    if ($this->osoby) {
      $osoby = $this->osoby;
      if ($osoby->count() == 0 ) {
        $this->flashMessage('Nenalezeny žádné osoby.', 'warning');
      }
      $this->template->akce = $this->osobaRepo->getOsobyAkce($osoby);
      $this->template->akceTypes = $this->akceRepo->fetchPossibleTypes();
    }
    else {
      $osoby = array();
    }
    $this->template->osoby = $osoby;
	}

	public function actionView($id = null) {  
    if ($id) {
      $osoba = $this->osobaRepo->findByID($id);
      
      if ($osoba) {
        $this->template->akce = $this->osobaRepo->getOsobaAkce($osoba);
        $this->template->akceTypes = $this->akceRepo->fetchPossibleTypes();
        $this->template->osoba = $osoba;
        $this->template->tymy = $this->tymRepo->findAll()
                                      ->where("kontaktni_osoba_id", $osoba->kontaktni_osoba_id);
        $this->template->potvrzenoTypy = $this->tymRepo->fetchPossiblePotvrzeno();
      }
      else {
        $this->flashMessage('Osoba #' . $id . ' nenalezena.', 'error');
      }
    }
    else {
      $this->flashMessage('Chybí ID', 'error');
    }    
	}

  public function osobaFormSubmitted (Nette\Forms\Controls\SubmitButton $submit) {
    $form = $submit->getForm();
    $values = $form->getValues();

    if(!$this->skolaRepo->findById($values['skola_id'])) {
      $this->flashMessage('Škola s takovýmto ID neexistuje', 'error');
      $form->setDefaults($values);
      return;
    }

    $osoba;
    if ($this->osoba) {
      $osoba = $this->osobaRepo->updateOsoba($values);
      $this->flashMessage('Osoba #' . $osoba->kontaktni_osoba_id . ' úspěšně upravena.', 'success');    
    }
    else {
      $osoba = $this->osobaRepo->insertOsoba($values);
      $this->flashMessage('Osoba #' . $osoba->kontaktni_osoba_id . ' úspěšně přidána.', 'success');    
    }
    $this->redirect('view', $osoba->kontaktni_osoba_id);
  }

	/**
	 * Osoba create/edit form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentOsobaForm() {
		$form = new Nette\Application\UI\Form;
    
    $form->getElementPrototype()->class = 'pure-form';

    $form->addGroup('Osobní údaje');
      $form->addRadioList('pohlavi', 'Pohlaví', array('žena', 'muž'))
            ->setRequired('"Pohlaví" je povinné pole.');
      $form->addText('titul_pred', 'Titul před jménem', 80);
      $form->addText('krestni', 'Křestní jméno', 80);
      $form->addText('prostredni', 'Prostřední Jméno', 80);
      $form->addText('prijmeni', 'Příjmení', 80);
      $form->addText('titul_za', 'Titul za jménem', 80);
      $form->addText('email', 'E-mail', 80);
      $form->addText('telefon', 'Telefon', 80);
      $form->addText('mobil', 'Mobil', 80);
      $form->addText('icq', 'ICQ', 80);
      $form->addText('skype', 'Skype', 80);
      $form->addTextArea('poznamka', 'Poznámka', 80, 5);
    
    $form->addGroup('Kontakt pro školu');
      $supp = $this->skolaRepo->findAll();
      $skoly = array(0 => '--- Zvolte školu ---');
      foreach($supp as $skola) {
        $skoly[$skola->skola_id] = $skola->skola_id . ' - '
                                      . $skola->nazev . ' '
                                      . $skola->mesto . ' '
                                      . $skola->ulice;   
      }
      $form->addSelect('skola_id', 'Škola osoby', $skoly)
            ->setRequired('"Škola osoby" je povinné pole.')
            ->addRule(Form::RANGE, 'Vyberte školu', array(1, $supp->max('skola_id')));
      $form->addText('pozice', 'Pozice', 80);
      $form->addRadioList('primarni', 'Primární', array('Ne', 'Ano'))
            ->setRequired('"Primární" je povinné pole.');
      $form->addSelect('kontaktni_osoba_typ_id', 'Typ', $this->osobaRepo->fetchPossibleTypes());

      $akceTypes = $this->akceRepo->fetchPossibleTypes();    
      $form->addMultiSelect('akce_typ', 'Kontakt pro akce', $akceTypes);

    $form->setCurrentGroup();   
    $form->addSubmit('save', 'Uložit')->onClick[] = array($this, 'osobaFormSubmitted');
    $form->addButton('cancel', 'Zrušit')->setAttribute('type','reset')
                                        ->setOmitted(true);
    
    if($this->osoba) {
      $form->addHidden('kontaktni_osoba_id');
      $osoba = $this->osoba->toArray();
      $osoba['akce_typ'] = $this->osobaRepo->getOsobaAkce($this->osoba);

      $form->setDefaults($osoba);
    }    

    return $form;
  }  

	public function actionEdit($id = null) {
    if ($id) {
      $osoba = $this->osobaRepo->findByID($id);
      
      if ($osoba) {
        $this->osoba = $osoba;
        $this->template->id = $id;
      }
      else {
        $this->flashMessage('Osoba #' . $id . ' nenalezena. Nelze editovat.', 'error');
      }
    }
    else {
      $this->flashMessage('Chybí ID', 'error');      
    }          
	}

	public function actionCreate() {
	}

}
