<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Diagnostics\Debugger;
use Nette\Forms\Controls\SubmitButton;
use Nette\Application\UI\Control;
use DateTime;
use DateInterval;
use DatePeriod;

/**
 * @author Mask
 */
class CinnostPresenter extends BasePresenter {

  protected $cinnost = null;

  public function cinnostFormSubmitted(Nette\Forms\Controls\SubmitButton $submit) {
    $form = $submit->getForm();
    $values = $form->getValues();
    $cinnost = $this->cinnost;

    if ($cinnost) {
      if ($cinnost->uzivatel_id != $this->currentUserId) {
        if (isset($values['akce_id'])) {
          unset($values['akce_id']);
        }
        if (isset($values['minuty'])) {
          unset($values['minuty']);
        }
        if (isset($values['datum'])) {
          unset($values['datum']);
        }
      }    
      $cinnost->update($values);
      $this->flashMessage('Změny byly úspěšně provedeny.', 'success');
    }
    else {
      $values['uzivatel_id'] = $this->currentUserId;
      $cinnost = $this->cinnostRepo->findAll()->insert($values);
      $this->flashMessage('Změny byly úspěšně provedeny.', 'success');      
    }
    $this->redirect('Cinnost:');
  }

  protected function createComponentCinnostForm() {
    $form = new Nette\Application\UI\Form;
    
    $form->getElementPrototype()->class = "pure-form";
    
    switch($this->getAction()) {
      case 'default':    
          $form->addGroup("Zapsat činnost");
          break;
      case 'edit':
          $form->addGroup("Upravit činnost #" . $this->cinnost->cinnost_id);
          break;
    }

    $akceBezici = $this->akceRepo->fetchRunning();
    $akceOptions = array();
    foreach ($akceBezici as $singleAkce) {
      $akceOptions[$singleAkce->akce_id]
              = $singleAkce->rok . " - " . $singleAkce->nazev;
    }
    
    $sekceOptions = $this->skupinaRepo->fetchForCinnosti()
                                      ->fetchPairs('skupina_id','nazev');
    
    $form->addSelect("akce_id", "Akce", $akceOptions);
    $form->addSelect("skupina_id","Sekce", $sekceOptions);
    $form->addText("datum", "Datum", 10, 10)
          ->setDefaultValue(date('Y-m-d'))
          ->setAttribute("class", "datum")
          ->addRule(  Form::PATTERN,
                      'Pole "Datum" musí být platné datum ve formátu RRRR-MM-DD',
                      '([0-9]\s*){4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])'
                    );
    $form->addText("minuty", "Minut", 4, 4)
          ->addRule(Form::RANGE, 'Pole "Minuty" musí být číslo od 0 do 1440.' , array("0","1440"));

    switch($this->getAction()) {
      case 'default':    
          $form->addTextArea("typ", "Cinnost", 20, 1);
          $form->addTextArea("popis", "Popis", 20, 1);
          break;
      case 'edit':
          $form->addTextArea("typ", "Cinnost", 40, 1);
          $form->addTextArea("popis", "Popis", 40, 4);
          break;
    }

    
    if ($this->cinnost) {
      $cinnost = $this->cinnost->toArray();
      $form->addHidden('cinnost_id');
      //pokud neni vlastnik, zablokuj "akce", "datum" a "minuty"
      if ($this->currentUserId != $cinnost['uzivatel_id']) {
        $form->getComponent("akce_id")->setDisabled(TRUE);
        $form->getComponent("datum")->setDisabled(TRUE);
        $form->getComponent("minuty")->setDisabled(TRUE);
      }                    
      $form->addSubmit("upravit", "Upravit")->onClick[] = array($this, 'cinnostFormSubmitted');
      $cinnost['datum'] = substr($cinnost['datum'], 0, 10);
      $form->setDefaults($cinnost);     
    }
    else {
      $form->addSubmit("ulozit", "Uložit")->onClick[] = array($this, 'cinnostFormSubmitted');    
    }
    
    return $form;
  }
  
  public function actionEdit($id = null) {
    if ($id) {
      $cinnost = $this->cinnostRepo->findById($id);
      if ($cinnost) {
        $akce = $this->akceRepo->findById($cinnost->akce_id);
        if ($akce->bezi != 1) {
          $this->flashMessage('Nelze editovat činnost #' . $id
                             . ', neboť patří pod akci "' . $akce->nazev
                            . '", která již byla ukončena.', 'error');      
        }

        $jeVlastnik = $this->currentUserId == $cinnost->uzivatel_id;
        if (!$jeVlastnik) {
          $jeVedouci = $this->cinnostRepo
                            ->isVedouci($this->currentUserId,
                                        $cinnost->cinnost_id);
          if(!$jeVedouci) {
            $this->flashMessage('Nemáte právo upravovat činnost #' . $id . '.', 'error');
          }      
        }
                
        if ($akce->bezi == 1 AND ($jeVlastnik OR $jeVedouci)) {
          $this->cinnost = $cinnost;
          $this->template->id = $id;          
        }
      }
      else {
        $this->flashMessage('Činnost #' . $id . " nenalezena.", 'error');      
      }
    }
    else {
      $this->flashMessage('Chybí ID', 'error');
    }
  }
  
  public function deleteFormSubmitted(Nette\Forms\Controls\SubmitButton $submit) {
    $cinnost = $this->cinnost;
    $id = $cinnost->cinnost_id;
    if ($cinnost) {
      $cinnost->delete();            
      $this->flashMessage('Činnost #' . $id . " smazána.", 'success');          
    }
    else {
      $this->flashMessage('Činnost #' . $id . " nenalezena.", 'error');      
    }    
    $this->redirect('Cinnost:');    	          
  }
  
  protected function createComponentDeleteOsobaForm() {
    $form = new Nette\Application\UI\Form;
    $form->getElementPrototype()->class = 'pure-form';
    $form->addSubmit('smazat', 'Smazat')->onClick[] = array($this,'deleteFormSubmitted');
    return $form;  
  }
  
  public function actionDelete($id = null) {
    if ($id) {
      $cinnost = $this->cinnostRepo->findById($id);
      if ($cinnost) {
        if($cinnost->akce_bezi) {
          if ($this->currentUserId == $cinnost->uzivatel_id) {
            $this->cinnost = $cinnost;
            $this->template->cinnost = $cinnost;        
          }
          else {
            $this->flashMessage('Cizí záznamy nelze mazat.', 'error');          
          }
        }
        else {
          $this->flashMessage('Akce ' . $cinnost->akce . ' již skončila. Činnost #' . $id . ' nelze smazat.', 'error');
        }
      }
      else {
        $this->flashMessage('Činnost #' . $id . " nenalezena.", 'error');      
      }
    }
    else {
      $this->flashMessage('Chybí ID', 'error');
    }
  }
  
  private function getLineGraphData($cinnosti) {
    $lineData = array();    

    //První a poslední den
    $init = reset($cinnosti); 
    $max = $init['datum'];
    $min = $init['datum'];
    foreach($cinnosti as $cinnost) {
      if ($cinnost['datum'] < $min) {
        $min = $cinnost['datum'];
      }
      elseif ($cinnost['datum'] > $max) {
        $max = $cinnost['datum'];
      }
    }
    
    $startDate = new DateTime($min);
    $this->template->startTimestamp = $startDate->getTimestamp();
    $endDate = new DateTime($max);       
    $obdobi = new DatePeriod($startDate, new DateInterval('P1D'), $endDate->modify('+1 day'));
    foreach ($obdobi as $den) {
      $lineData[$den->format('Y-m-d')] = 0;
    }
    foreach($cinnosti as $cinnost) {
      $lineData[$cinnost->datum->format('Y-m-d')] += $cinnost->minuty;      
    }
    return $lineData;  
  }

  private function getPieGraphData($cinnosti) {
    $pieData = array();
    foreach($cinnosti as $cinnost) {
      if(isset($pieData[$cinnost->skupina_id])) {
        $pieData[$cinnost->skupina_id][1] += $cinnost->minuty; 
      }
      else {
        $pieData[$cinnost->skupina_id] = array($cinnost->skupina, $cinnost->minuty);               
      }
    }
    return $pieData;  
  }

  public function cinnostSearchFormSubmitted(Nette\Forms\Controls\SubmitButton $submit) {
    $form = $submit->getForm();
    $values = $form->getValues();
    
    $asSelf = $this->cinnostRepo->findAll();
    $asVedouci = $this->cinnostRepo->findAsVedouciByUserId($this->currentUserId);
    
    $akce = $this->userRepo->getAkceSetting($this->currentUserId);
    if(!empty($akce)) {
      $asSelf->where('cinnost.akce_id', $akce);    
      $asVedouci->where('cinnost.akce_id', $akce);    
    }
    if(isset($values['skupina_id']) && !empty($values['skupina_id'])) {
      $asSelf->where('cinnost.skupina_id', $values['skupina_id']);    
      $asVedouci->where('cinnost.skupina_id', $values['skupina_id']);    
    }
    if(isset($values['od']) && !empty($values['od'])) {
      $asSelf->where('cinnost.datum >=', $values['od']);
      $asVedouci->where('cinnost.datum >=', $values['od']);
    }
    if(isset($values['do']) && !empty($values['do'])) {
      $asSelf->where('cinnost.datum <=', $values['do']);
      $asVedouci->where('cinnost.datum <=', $values['do']);
    }
    if(isset($values['uzivatel_id']) && !empty($values['uzivatel_id'])) {
      $asVedouci->where('cinnost.uzivatel_id', $values['uzivatel_id']);    
      $supp[0] = $asVedouci->fetchPairs('cinnost_id'); 
      if (in_array($this->currentUserId, $values['uzivatel_id'])) {
        $asSelf->where('cinnost.uzivatel_id', $this->currentUserId);
        $supp[1] = $asSelf->fetchPairs('cinnost_id');
        $cinnosti = array();
        foreach ($supp[0] as $row) {
          $cinnosti[$row->cinnost_id] = $row;
        } 
        foreach ($supp[1] as $row) {
          $cinnosti[$row->cinnost_id] = $row;
        } 
        uasort($cinnosti, function($a,$b)
            {
              $result = (-1) * strcmp($a['datum'], $b['datum']);
              if ($result == 0) {
                if ($a['cinnost_id'] != $b['cinnost_id']) {
                  if ($a['cinnost_id'] > $b['cinnost_id']) {
                    $result = -1;
                  }
                  else {
                    $result = 1;
                  }
                }
              }
              return $result;
            }
        );
      }
      else {
        $cinnosti = $supp[0];
      }
      $this->template->cinnosti = $cinnosti;

      $sum = 0;
      foreach ($cinnosti as $cinnost) {
        $sum += $cinnost->minuty;
      }
      $this->template->sum = $sum;

      if (count($cinnosti) > 0 ) {
        $this->template->lineData = $this->getLineGraphData($cinnosti);        
        $this->template->pieData = $this->getPieGraphData($cinnosti);
      }
    }
    
  }
  
  protected function createComponentCinnostSearchForm() {
    $form = new Nette\Application\UI\Form;
    
    $form->getElementPrototype()->class = "pure-form";
    $form->addGroup("Hledat činnosti");

    $sekceOptions = $this->skupinaRepo->fetchForCinnosti()
                                      ->order('nazev')
                                      ->fetchPairs('skupina_id', 'nazev');
    $form->addMultiSelect('skupina_id','Sekce', $sekceOptions, 1)
          ->setDefaultValue(array_keys($sekceOptions));

    $uzivatelOptions = $this->userRepo->findAll()
                                      ->order('login')
                                      ->fetchPairs('uzivatel_id','login');
    $form->addMultiSelect('uzivatel_id','Uzivatel', $uzivatelOptions, 1)
          ->setDefaultValue($this->currentUserId);
    $form->addText('od', 'Od')
          ->setDefaultValue(date('Y-m-d', mktime(0,0,0,1,1,2012)))
          ->setAttribute("class", "datum")
          ->addCondition(Form::FILLED)
          ->addRule(  Form::PATTERN,
                      'Pole "Od" musí být platné datum ve formátu RRRR-MM-DD',
                      '([0-9]\s*){4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])'
                    );
    $form->addText('do', 'Do')
          ->setDefaultValue(date('Y-m-d'))
          ->setAttribute("class", "datum")
          ->addCondition(Form::FILLED)
          ->addRule(  Form::PATTERN,
                      'Pole "Do" musí být platné datum ve formátu RRRR-MM-DD',
                      '([0-9]\s*){4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])'
                    );
    $form->addSubmit('hledat', 'Hledat')->onClick[] = array($this, 'cinnostSearchFormSubmitted');
    return $form;
  }

  public function actionDefault() {
    $uzivatel_id = $this->currentUserId;
    $selectedAkce = $this->userRepo->getAkceSetting($uzivatel_id);
    $cinnosti = $this->cinnostRepo->findByUserId($uzivatel_id);
    if (!empty($selectedAkce)) {
      $cinnosti = $cinnosti->where('cinnost.akce_id', $selectedAkce);
    }
    $cinnosti = $cinnosti->fetchPairs('cinnost_id');    
    $this->template->cinnosti = $cinnosti;

    $sum = 0;
    foreach ($cinnosti as $cinnost) {
      $sum += $cinnost->minuty;
    }
    $this->template->sum = $sum;

    if (count($cinnosti) > 0 ) {
      $this->template->lineData = $this->getLineGraphData($cinnosti);     
      $this->template->pieData = $this->getPieGraphData($cinnosti);
    }
  }
  
}