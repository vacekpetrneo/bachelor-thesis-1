<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI\Form;
use Nette\Diagnostics\Debugger;
use Nette\Forms\Controls\SubmitButton;
use Nette\Application\UI\Control;

/**
 * @author Mask
 */
class TurnajPresenter extends BasePresenter {

  protected $turnajId = null;

  /**
   * (non-phpDoc)
   *
   * @see Nette\Application\Presenter#startup()
   */
  protected function startup() {
    parent::startup();
    switch ($this->getAction) {
      case 'default' : 		
      case 'view' :
        $this->checkRights('TURNAJE-READ' );
        break;
      case 'create' :
      case 'edit' :
      case 'removetym' :
      case 'vysledky' :
        $this->checkRights('TURNAJE-WRITE');
        break;
    }
  }
  
  public function actionView($id = null) {
    if ($id) {
      $turnaj = $this->turnajRepo->findByID($id);
      
      if ($turnaj) {
        $this->template->turnaj = $turnaj;
        $stavy = $this->turnajRepo->fetchPossibleStav();
        $this->template->stav = $stavy[$turnaj->stav];                 
        $this->template->tymy = $this->tymRepo->getTymsByTurnajId($id);
        $this->template->tymPotvrzeni = $this->tymRepo->fetchPossiblePotvrzeno();


        $vysledky = $this->turnajRepo->getVysledkyByTurnajId($id)
                                      ->fetchPairs('tym_id','poradi');
        $supp = array();
        foreach($vysledky as $tym_id => $vysledek) {
          $supp[$vysledek][] = true;
        }
        $poradi = array();
        foreach($vysledky as $tym_id => $vysledek) {
          $poradi[$tym_id] = $vysledek;
          if (count($supp[$vysledek]) > 1) {
            $poradi[$tym_id] = $poradi[$tym_id] . ' - ' . ($vysledek + count($supp[$vysledek]) - 1);
          }
        }
                                    
        Debugger::barDump($poradi);
        $this->template->poradi = $poradi;
      }
      else {
        $this->flashMessage('Turnaj #' . $id . ' nenalezen.', 'error');
      }
    }    
    else {
      $this->flashMessage('Chybí ID', 'error');
    }    
  }

  public function actionCreatereport($id = null) {
    if ($id) {
      $turnaj = $this->turnajRepo->findByID($id);      
      
      if ($turnaj) {
        if ($turnaj->novinka_id) {
          $this->flashMessage('Turnaj #' . $id . ' již má vytvořenou svoji novinku', 'forward');
          $this->redirect('Novinka:view', $turnaj->novinka_id);    	      
        }
        else {
          $this->checkRights('NOVINKY-WRITE');
          $novinka = array( 'uzivatel_id' => $this->getUser()->getId(),
                            'kategorie_id' => 5,
                            'akce_id' => $turnaj->akce_id,
                            'nazev' => 'Report z turnaje - doplnit',
                          );
          if($turnaj->galerie_id) {
            $novinka['galerie_id'] = $turnaj->galerie_id; 
          }
      
          $skola = $this->skolaRepo->findByID($turnaj->skola_id);
          $novinka['region_id'] = $skola->region_id;        
      
          $turnaj->update(array('novinka_id' => $this->novinkaRepo->insert($novinka)->novinka_id));
          $this->redirect('Novinka:edit', $turnaj->novinka_id);    	      
        }
      }
      else {
        $this->flashMessage('Turnaj #' . $turnajId . ' nenalezen.', 'error');      
      }      
    }
    else {    
      $this->flashMessage('Chybí ID', 'error');
    }
  }

  public function vysledkyFormSubmitted(Nette\Forms\Controls\SubmitButton $submit) {
    $form = $submit->getForm();
    $values = $form->getValues();

    $vysledkyOld = $this->turnajRepo->getVysledkyByTurnajId($this->turnajId)
                                    ->fetchPairs('tym_id','poradi');
    foreach($values as $elementId => $poradi) {
      $tym_id = substr($elementId, 3);
      if($poradi == 0) {
        $this->turnajRepo->deleteVysledek($this->turnajId, $tym_id);        
      }
      else {
        if(isset($vysledkyOld[$tym_id])) {
          $this->turnajRepo->updateVysledek($this->turnajId, $tym_id, $poradi);                  
        }
        else {
          $this->turnajRepo->insertVysledek($this->turnajId, $tym_id, $poradi);                          
        }
      }
    }
    
    $this->redirect("Turnaj:view", $this->turnajId);
  }
  
  protected function createComponentVysledkyForm() {
    $form = new Nette\Application\UI\Form;
  
    $form->getElementPrototype()->class = 'pure-form';
    $form->addGroup('Upravit výsledky');

    $tymy = $this->tymRepo->getTymsByTurnajId($this->turnajId);
    $tymCount = $tymy->count();
    $vysledky = $this->turnajRepo->getVysledkyByTurnajId($this->turnajId)
                                  ->fetchPairs('tym_id','poradi');
    $options[0] = 'Neuvedeno';
    $options = $options + range(0, $tymCount);    

    $poradi = array();
    foreach($tymy as $tym) {
      if (isset($vysledky[$tym->tym_id])) {
        $poradi[$vysledky[$tym->tym_id]][] = $tym;       
      }
      else {
        $poradi[0][] = $tym;
      }
    }
    ksort($poradi);

    foreach($poradi as $poradi => $tymy) {
      foreach($tymy as $tym) {
        $label = 'POŘADÍ NEUVEDENO : ';
        if ($poradi != 0) {
          $label = $poradi;
          if (count($tymy) > 1) {
            $label = $label . '. - ' . ($poradi + count($tymy) - 1);
          }
          $label = $label . '. místo: ';
        }
        $label = $label . '"' . $tym->nazev . '" - ' . $tym->skola_nazev;
        $form->addSelect('tym' . $tym->tym_id, $label, $options)
              ->setDefaultValue($poradi);
      }
    }
    $form->addSubmit('save', 'Uložit')->onClick[] = callback($this, 'vysledkyFormSubmitted');
    return $form;  
  }
  
  public function actionVysledky ($id = null) {
    if ($id) {
      $turnaj = $this->turnajRepo->findById($id);
      
      if($turnaj) {
        $this->turnajId = $id;
        $this->template->id = $id;

        $vysledky = $this->tymRepo->getTymsByTurnajId($id);
        $tymCount = $vysledky->count();
        if ($tymCount == 0) { 
          $this->flashMessage('Na turnaji nejsou žádné týmy','error');
        }

        $this->template->tymCount = $tymCount;        
      }
      else {
        $this->flashMessage('Turnaj #' . $id . ' nenalezen', 'error');      
      }
    }
    else {
      $this->flashMessage('Chybí ID', 'error');  
    } 
  }
  
  public function turnajFormSubmitted(Nette\Forms\Controls\SubmitButton $submit) {
    $form = $submit->getForm();
    $success = true;

    $values = $form->getValues();
    if ($values['uzivatel_id'] == 0) {
      $this->flashMessage('Zvolte organizátora', 'error');          
      $success = false;  
    }
    else {
      $user = $this->userRepo->findById($values['uzivatel_id']);
      if (!$user) {
        $this->flashMessage('Vámi zvolený organizátor neexistuje, vyberte existujícího organizátora', 'error');          
        $success = false;          
      }
    }
    if ($values['uroven_id'] == 0) {
      $this->flashMessage('Zvolte úroveň', 'error');          
      $success = false;  
    }
    else {
      $uroven = $this->urovenRepo->findById($values['uroven_id']);
      if (!$uroven) {
        $this->flashMessage('Vámi zvolená úroveň neexistuje, vyberte existující úroveň.', 'error');
        $success = false;          
      }    
    }    
    if ($values['skola_id'] == 0) {
      $this->flashMessage('Zvolte školu', 'error');                    
      $success = false;  
    }
    else {    
      $skola = $this->skolaRepo->findById($values['skola_id']);
      if (!$skola) {
        $this->flashMessage('Vámi zvolená škola neexistuje, vyberte existující školu.', 'error');
        $success = false;
      }
    } 
    if ($values['kraj_id'] == 0) {
      if ($values['skola_id'] != 0) {
        $skola = $this->skolaRepo->findById($values['skola_id']);
        if ($skola) {
          $values['kraj_id'] = $skola->kraj_id;
        }
      }          
    }
    else {
      //FIXME, TODO Kontrola, zda zvolený kraj existuje
    }    
    if ($success) {
      switch ($submit->getName()) {
        case 'zmenit':
            $turnaj = $this->turnajRepo->findById($this->turnajId);
            if ($turnaj->update($form->getValues())) {
              $this->flashMessage('Turnaj byl změněn', 'success');
              $this->redirect("Turnaj:view", $turnaj->turnaj_id);
            }
            break;
        case 'vytvorit':
            $values['stav'] = 0;
            $values['galerie_id'] = $this->turnajRepo->createGalerie();
            $turnaj = $this->turnajRepo->findAll()->insert($values);
            if ($turnaj) {          
              $this->flashMessage('Turnaj byl vytvořen', 'success');
              $this->redirect("Turnaj:view", $turnaj->turnaj_id);
            }
            break;
        default:  
            $this->flashMessage('Chyba v odeslaném formuláři', 'error');
      }
    }
  }

  public function createComponentTurnajForm() {
    $form = new Nette\Application\UI\Form;
    
    $form->getElementPrototype()->class = 'pure-form';

    switch ($this->getAction()) {
      case 'create':
          $form->addGroup('Vytvořit nový turnaj');
          break;
      case 'edit':
          $form->addGroup('Upravit turnaj #' . $this->turnajId);
          break;
    }
        
    $urovne = array(0 => "--- Zvolte úroveň ---");
    $table = $this->urovenRepo->findAll()
                              ->order("akce.rok DESC, akce.akce_id DESC, uroven.uroven_id");
    if (!$this->turnajId){
      $table->where("akce.bezi = 1");
    }
    foreach($table as $row) {
      $urovne[$row->uroven_id] = $row->akce_nazev . " - " . $row->nazev;
    }
    
    $uzivatele = array(0 => "--- Zvolte organizátora ---");
    $table = $this->userRepo->findAll()->order("login");
    foreach ($table as $row) {
      $uzivatele[$row->uzivatel_id] = $row->login . " - "
                                     . $row->krestni . " " . $row->prijmeni;
    }

    $okres = $this->okresRepo->findAll()->fetchPairs("okres_id", "nazev");

    $skoly = array(0 => "--- Zvolte školu ---");
    $table = $this->skolaRepo->findAll()
                              ->order("skola.okres_id, skola.mesto, skola.nazev");
    foreach ($table as $row) {
      $skoly[$okres[$row->okres_id]][$row->skola_id] = 
            $row->mesto . " - " . $row->nazev . " - " . $row->ulice;
    }   
    
    $kraje = array(0 => "Podle školy") + $this->krajRepo->fetchPossible();
    
    $form->addSelect("uzivatel_id", "Organizátor", $uzivatele)
          ->addRule(Form::FILLED, "Zvolte organizátora");
    $form->addSelect("uroven_id", "Úroveň", $urovne)
          ->addRule(Form::FILLED, "Zvolte úroveň");
    $form->addSelect("kraj_id", "Kraj", $kraje)
          ->addRule(Form::FILLED, "Zvolte kraj");
    $form->addSelect("skola_id", "Pořadatelská škola", $skoly)
          ->addRule(Form::FILLED, "Zvolte pořadatelskou školu");
    $form->addText("cas", "Datum a čas", 16, 16)
          ->setAttribute("class", "datumCas")
          ->setValue(date("Y-m-d") . " 08:30:00")
          ->addRule(Form::FILLED, "Zvolte datum a čas");
    $form->addText("max_tymu", "Maximální počet týmů")
          ->setValue(666)
          ->addRule(Form::FILLED, "Zadejte maximální počet týmů");
    $form->addText("postupujici", "Počet postupujících týmů")
          ->setValue("1")
          ->addRule(Form::FILLED, "Zadejte počet postupujících týmů");
    $form->addTextArea("info", "Informace", 60, 5);
    
    if ($this->turnajId) {
      $turnaj = $this->turnajRepo->findById($this->turnajId);
      $stavy = $this->turnajRepo->fetchPossibleStav();
      $form->addSelect("stav", "Stav", $stavy);
      $form->setDefaults($turnaj);
      $form->addSubmit("zmenit", "Změnit")
            ->onClick[] = array($this, 'turnajFormSubmitted');    
    } 
    else {
      $form->addSubmit("vytvorit", "Vytvořit")
            ->onClick[] = array($this, 'turnajFormSubmitted');
    }

    return $form;
  }
  
  public function actionCreate() {}
  
  public function actionEdit($id = null) {
    if ($id) {
      $turnaj = $this->turnajRepo->findByID($id);
      
      if ($turnaj) {
        $this->turnajId = $id;
        $this->template->id = $id;
        $this->template->puvodniStav = $turnaj->stav;
      }
      else {
        $this->flashMessage('Turnaj #' . $id . ' nenalezen.', 'error');
      }
    }    
    else {
      $this->flashMessage('Chybí ID', 'error');
    }  
  }

  public function turnajSearchFormSubmitted(Nette\Forms\Controls\SubmitButton $submit) {
    $form = $submit->getForm();
    $values = $form->getValues();
    $turnaje = $this->turnajRepo->findAll();
    $zvoleneAkce = $this->userRepo->getAkceSetting($this->currentUserId);
    $turnaje->where('uroven.akce_id', $zvoleneAkce);
    if(isset($values['uroven_id']) && !empty($values['uroven_id'])) {
      $turnaje->where('turnaj.uroven_id', $values['uroven_id']);
    }
    if(isset($values['region_id']) && !empty($values['region_id'])) {
      $turnaje->where('kraj:kraj_vazba.region_id', $values['region_id']);
    }
    if(isset($values['stav']) && !empty($values['stav'])) {
      $turnaje->where('turnaj.stav', $values['stav']);
    }
    if(isset($values['od']) && !empty($values['od'])) {
      $turnaje->where('turnaj.cas >=', $values['od']);
    }
    if(isset($values['do']) && !empty($values['do'])) {
      $turnaje->where('turnaj.cas <=', $values['do']);
    }
    $this->template->turnaje = $turnaje;
  }
  
  protected function createComponentTurnajSearchForm() {
    $form = new Nette\Application\UI\Form;
    $form->getElementPrototype()->class = 'pure-form';
    $form->addGroup('Hledat turnaje');

    $possibleStavy = $this->turnajRepo->fetchPossibleStav();
     
    $republiky = $this->regionTree->getChildren($this->regionTree->getRegionById(1));
    $regions = array();
    foreach ($republiky as $republika) {
      $regions['Státy'][$republika['region_id']] = $republika['nazev'];
      $kraje = $this->regionTree->getChildren($republika);
      foreach ($kraje as $kraj) {
        $regions[$kraj['nazev']][$kraj['region_id']] = $kraj['nazev'];
        $okresy = $this->regionTree->getChildren($kraj);
        foreach ($okresy as $okres) {
          $regions[$kraj['nazev']][$okres['region_id']] = $okres['nazev'];
        }
      }
    }

    $urovne = $this->urovenRepo->getUrovneByAkceId($this->userRepo->getAkceSetting($this->currentUserId))
                                ->fetchPairs('uroven_id');
    foreach ($urovne as $uroven_id => $uroven) {
      $urovne[$uroven_id] = $uroven['nazev'] . ' - ' . $uroven['akce_nazev'];
    }
    $form->addMultiSelect('uroven_id', 'Úrovně', $urovne, 1)
          ->setDefaultValue(array_keys($urovne));    
    
    $form->addMultiSelect('region_id', 'Region', $regions, 1);    
    $form->addMultiSelect('stav', 'Stav', $possibleStavy, 1)
          ->setDefaultValue(array_keys($possibleStavy));    
    $form->addText('od', 'Od')
          ->setDefaultValue(date('Y-m-d', mktime(0,0,0,1,1,2005)))
          ->setAttribute("class", "datum")
          ->addCondition(Form::FILLED)
          ->addRule(  Form::PATTERN,
                      'Pole "Od" musí být platné datum ve formátu RRRR-MM-DD',
                      '([0-9]\s*){4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])'
                    );
    $form->addText('do', 'Do')
          ->setDefaultValue(date('Y-m-d'))
          ->setAttribute("class", "datum")
          ->addCondition(Form::FILLED)
          ->addRule(  Form::PATTERN,
                      'Pole "Do" musí být platné datum ve formátu RRRR-MM-DD',
                      '([0-9]\s*){4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])'
                    );
    $form->addSubmit("hledat", "Hledat")->onClick[]
            = array($this, 'turnajSearchFormSubmitted');
    return $form;  
  }
  
  public function actionDefault() {
    $this->template->stavy = $this->turnajRepo->fetchPossibleStav();
    $zvoleneAkce = $this->userRepo->getAkceSetting($this->currentUserId);
    $turnaje = $this->turnajRepo->findAll()->where('uroven.akce_id', $zvoleneAkce);
    $this->template->turnaje = $turnaje;    

    $turnajIds = array();
    foreach ($turnaje as $turnaj) {
      $turnajIds[] = $turnaj->turnaj_id;
    }
  }
  
}
