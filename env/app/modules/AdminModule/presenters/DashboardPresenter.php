<?php

namespace AdminModule;

use Nette;

/**
 * Description of Dashboard
 *
 * @author rattus
 */
class DashboardPresenter extends BasePresenter {

	/**
	 * (non-phpDoc)
	 *
	 * @see Nette\Application\Presenter#startup()
	 */
	protected function startup() {
		parent::startup();
	}

	public function renderDefault() {
		
	}

}