<?php

namespace AdminModule;

use Nette\Application\UI;

/**
 * Desne promakany vybiratko akci
 * @author Rattus
 */
class AkceSelektor extends UI\Control {

    /** @var \SCG\AkceRepository */
    private $akceRepository;

    /** @var \SCG\UserRepository */
    private $userRepository;
    
    public function setAkceRepo(\SCG\AkceRepository $repo) {
        $this->akceRepository = $repo;
    }
    
    public function setUserRepo(\SCG\UserRepository $repo) {
        $this->userRepository = $repo;
    }

    public function renderJs() {
        if ($this->presenter->getUser()->isLoggedIn()) {
            $this->template->setFile(__DIR__ . '/AkceSelektorJs.latte');
            $this->template->typyAkci = $this->akceRepository->fetchPossibleTypes();
            $this->template->render();
        }
    }

    public function render() {
        if ($this->presenter->getUser()->isLoggedIn()) {
            $this->template->setFile(__DIR__ . '/AkceSelektor.latte');
            $this->template->render();
        }
    }

    public function createComponentAkceSelectForm() {
        if ( !$this->userRepository instanceof \SCG\UserRepository ) {
            throw new \InvalidArgumentException("UserRepository not set for AkceSelektor");
        }
        if ( !$this->akceRepository instanceof \SCG\AkceRepository ) {
            throw new \InvalidArgumentException("AkceRepository not set for AkceSelektor");
        }
        $form = new UI\Form;

        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = NULL;
        $renderer->wrappers['label']['container'] = NULL;
        $renderer->wrappers['control']['container'] = NULL;
        $renderer->wrappers['hidden']['container'] = NULL;

        $akceValue = $this->userRepository->getAkceSetting( $this->getPresenter(TRUE)->getUser()->getId() );
        if ( is_array( $akceValue ) ) {
            $akceValue = implode( ',', $akceValue );
        }

        $form->getElementPrototype()->class = 'pure-form';
        $form->addHidden('akce_id' )
                ->setAttribute('class', 'akce-selektor-select')
                ->setAttribute('title', 'Výběr akce')
                ->setAttribute('data-options', json_encode( $this->akceRepository->fetchPossible(TRUE) ) )
                ->setDefaultValue( $akceValue );

        $form->addSubmit('hledej', 'Nastav')->setAttribute('class', 'pure-button pure-button-primary');

        // call method signInFormSucceeded() on success
        $form->onSuccess[] = callback($this, 'processAkceForm');
        return $form;
    }

    public function processAkceForm(UI\Form $form) {
        $values = $form->values;
        
        $this->userRepository->setAkceSetting( 
            $this->getPresenter(TRUE)->getUser()->getId(),
            explode( ',', $values['akce_id'])
        );
        
        $this->flashMessage('Akce nastavena.');
        $this->redirect('this');
    }

}
