<?php

namespace AdminModule;

use Nette\Application\UI\Control;

/**
 * Description of AdminMenu
 *
 * @author rattus
 */
class AdminMenu extends Control {
	
	public function render() {
		$this->template->presenter = $this->getPresenter( true );
		$this->template->user = $this->template->presenter->getUser();
    $this->template->db = $this->template->presenter->getCurrentDB();
		$this->template->setFile(dirname(__FILE__) . '/AdminMenu.latte');
		
		$this->template->render();
	}
	
}
