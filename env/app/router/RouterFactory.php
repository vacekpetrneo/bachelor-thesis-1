<?php

namespace SCG;

use Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\IRouter;

/**
 * Router factory.
 */
class RouterFactory {

	/**
	 * @return \Nette\Application\IRouter
	 */
	public static function createRouter() {
		$router = new RouteList();
    $router[] = new Route('turnaj/removetym[/turnaj/[<turnajId>]][/tym/[<tymId>]]', 'Admin:Turnaj:removetym');
    $router[] = new Route('turnaj/removetym[/tym/[<tymId>]][/turnaj/[<turnajId>]]', 'Admin:Turnaj:removetym');
		$router[] = new Route('<module=Admin>/<presenter>/<action>[/<id>]', 'Dashboard:default');
		return $router;
	}

}
