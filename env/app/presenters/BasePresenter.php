<?php

namespace SCG;

use Nette,	Nette\Diagnostics\Debugger;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {
	
	/** @var AkceRepository */
	protected $akceRepo;
	
	/** @var CinnostRepository */
	protected $cinnostRepo;
	
	/** @var KrajRepository */
	protected $krajRepo;
	
  /** @var NovinkaRepository */
  protected $novinkaRepo;
  
	/** @var OkresRepository */
	protected $okresRepo;
	
	/** @var OsobaRepository */
	protected $osobaRepo;
  
	/** @var RegionTree */
	protected $regionTree;
	
	/** @var SkolaRepository */
	protected $skolaRepo;
	
	/** @var SkupinaRepository */
	protected $skupinaRepo;
	
	/** @var TurnajRepository */
	protected $turnajRepo;
	
  /** @var TymRepository */
  protected $tymRepo;
	
  /** @var UrovenRepository */
  protected $urovenRepo;
	
  /** @var UserRepository */
  protected $userRepo;
	
	/**
	 * (non-phpDoc)
	 *
	 * @see Nette\Application\Presenter#startup()
	 */
	protected function startup() {
		parent::startup();

		if (Debugger::isEnabled()) {
			Nette\Extras\Debug\RequestsPanel::register();
		}
    
    // close session on exit
    register_shutdown_function('session_write_close');
	}
	
	public function injectAkceRepository( AkceRepository $akceRepo ) {
		$this->akceRepo = $akceRepo;
	}
	
	public function injectCinnostRepository( CinnostRepository $cinnostRepo ) {
		$this->cinnostRepo = $cinnostRepo;
	}
	
	public function injectKrajRepository( KrajRepository $krajRepo ) {
		$this->krajRepo = $krajRepo;
	}
	
  public function injectNovinkaRepository(NovinkaRepository $novinkaRepo ) {
		$this->novinkaRepo = $novinkaRepo;
	}
	
	public function injectOkresRepository( OkresRepository $okresRepo ) {
		$this->okresRepo = $okresRepo;
	}
	
	public function injectOsobaRepository( OsobaRepository $osobaRepo ) {
		$this->osobaRepo = $osobaRepo;
	}
  
	public function injectRegionTree( RegionTree $regionTree ) {
		$this->regionTree = $regionTree;
	}
	
	public function injectSkolaRepository( SkolaRepository $skolaRepo ) {
		$this->skolaRepo = $skolaRepo;
	}
	
	public function injectSkupinaRepository( SkupinaRepository $skupinaRepo ) {
		$this->skupinaRepo = $skupinaRepo;
	}
	
	public function injectTurnajRepository( TurnajRepository $turnajRepo ) {
		$this->turnajRepo = $turnajRepo;
	}
	
  public function injectTymRepository( TymRepository $tymRepo ) {
    $this->tymRepo = $tymRepo;
  }
  
  public function injectUserRepository( UserRepository $userRepo ) {
    $this->userRepo = $userRepo;
  }
  
  public function injectUrovenRepository( UrovenRepository $urovenRepo ) {
    $this->urovenRepo = $urovenRepo;
  }
  
}
