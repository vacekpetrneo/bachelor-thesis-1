<?php

namespace SCG;

use Nette\Diagnostics\Debugger;

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

	public function renderDefault()
	{
		$user = $this->getUser();
		if ( $user->isLoggedIn() ) {
			if ( $user->isAllowedForAkce( 12, Nette\Security\IAuthorizator::ALL, 'KONTAKTNIOSOBY-READ') ) {
				Debugger::barDump( "ano", "Opravnen" );
			}
			Debugger::barDump( $user, "Test" );
		}
		
		$this->template->user = $user;
	}

}
