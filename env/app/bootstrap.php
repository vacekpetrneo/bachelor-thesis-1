<?php

// Load Nette Framework or autoloader generated by Composer
require dirname(__FILE__) . '/../libs/Nette/loader.php';

$configurator = new Nette\Config\Configurator;

// Enable Nette Debugger for error visualisation & logging
//$configurator->setDebugMode(TRUE);
$configurator->setDebugMode( TRUE ); $configurator->addParameters( array( 'environment' => Nette\Config\Configurator::DEVELOPMENT ) );
$configurator->enableDebugger(dirname(__FILE__) . '/../log');

// Specify folder for cache
$configurator->setTempDirectory(dirname(__FILE__) . '/../temp');

// Enable RobotLoader - this will load all classes automatically
$configurator->createRobotLoader()
	->addDirectory(dirname(__FILE__))
	->addDirectory(dirname(__FILE__) . '/../libs')
	->register();

// not needed anymore
// Nette\Forms\Controls\CheckboxList::register();

// Create Dependency Injection container from config.neon file
$configurator->addConfig(dirname(__FILE__) . '/config/config.neon');
$configurator->addConfig(dirname(__FILE__) . '/config/config.local.neon', false ); // none section
$container = $configurator->createContainer();

return $container;
