This is my bachelor thesis that I successfully defended in 2014 at 
[Masaryk University](https://www.muni.cz/en) in Brno, Czech Republic. It's an 
app made with [Nette Framework](https://nette.org/en/) for a nonprofit 
organization [Student Cyber Games](https://scg.cz/) to help them organize 
tournaments for students of age 12-20.

Structure
======================
The problem domain, analysis, design and some implementation details are 
documented in Czech language in the bachelor-thesis-1.pdf file.

The "BP" folder contains source codes that I have created as a part of the 
bachelor thesis and that implement the use cases as they are described and 
designed in the acompanying PDF document. Few of the included methods were 
authored by [Petr Vítek](https://www.linkedin.com/in/vitekp). Such methods are 
properly anotated with `@author Petr Vitek` string. All of the remaining code is 
my original work. The purpose of the "BP" folder is to separate my own work as
clearly as possible.

The whole of application runtime is situated in "env" folder, including the 
source codes from the "BP" folder and the unified work of the development team 
of [Student Cyber Games](https://scg.cz/) non-profit organisation as well as 
other authors whose code is being used by the application (libraries etc).

Requirements
======================
The included source code is meant to be used with Apache HTTP server 2.2 with 
working PHP 5.3 module. Furthermore, MySQL 5.5 database is needed. For correct 
app operation it is necessary to use Apache with properly configured 
mod_rewrite module. The following modules also need to be enabled in PHP 
settings:
php_bz2.dll
php_fileinfo.dll
php_gd2.dll
php_mbstring.dll
php_mysql.dll
php_pdo_mysql.dll

Running the app
======================
If you wish to try the app out, follow these steps:
1) In your DBMS, run the queries from file "createSampleData.sql". A database 
named "test" will be created as will a user named "test" to whom all permissions 
to the said database will be granted. After that, tables essential to run the 
app will be created and populated with testing data. 
2) Upload the "env" folder to your HTTP server. It is expected that the server 
and the DB run on the same machine, thus the database should be accessible via 
"localhost" address. If your database is elsewhere, you need to edit the file 
"env/app/config/config.local.neon".
3) Start the HTTP server
4) Send a HTTP request to "env/www/index.php"
5) To login to the app you can use "test" username (exclude quotes) with "test" 
password (exclude quotes)

TODOs
======================
Please, bear in mind that this is an ancient project and many modern features 
are missing including:
- Dockerization
- Usage of composer to maintain libraries
- Support for newest PHP version
- Utilization of modern PHP features like type hinting
- Uniform coding standards enforced by e.g. PHP_CodeSniffer
- Frontend successor to jQuery
- Better separation of concerns (model should not pass table object to Controller - eew! )
